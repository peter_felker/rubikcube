package com.jogl.rubik;

import com.jogl.rubik.game.GameWindow;
import com.jogl.rubik.util.window.WindowUtils;

public class Main {
    
    public static void main(String[] args) {
        GameWindow gameWindow = new GameWindow();
        WindowUtils.moveToScreenCenter(gameWindow);
        gameWindow.initGameArea();
        gameWindow.setVisible(true);
    }
    
}
