package com.jogl.rubik.util.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.jogl.rubik.math.NumberUtils;

public class CollectionUtil {

    private CollectionUtil() { }
    
    public static float[] toFloatArray(Collection<Float> floatCollection) {
        float[] floatArr = new float[floatCollection.size()];
        int i=0;
        for (Float f : floatCollection) {
            floatArr[i] = f.floatValue();
            i++;
        }
        
        return floatArr;
    }
    
    public static List<Float> toFloatList(float[] floatArr) {
        List<Float> floatList = new LinkedList<>();
        
        for (int i = 0; i < floatArr.length; i++) {
            floatList.add(floatArr[i]);
        }
        
        return floatList;
    }

    public static <T> T getRandomElem(Collection<T> collection) {
        int randIndex = NumberUtils.randInt(0, collection.size()-1);
        List<T> list = new ArrayList<>(collection);
        return list.get(randIndex);
    }
    
}
