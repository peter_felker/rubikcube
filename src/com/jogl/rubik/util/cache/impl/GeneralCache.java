package com.jogl.rubik.util.cache.impl;

import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;
import java.util.function.Supplier;

import com.jogl.rubik.util.cache.Cache;
import com.jogl.rubik.util.cache.Key;

public class GeneralCache<V> implements Cache<Key, V> {

    private Map<Key, V> cache = new WeakHashMap<>();
    private final Object cacheModificationLock = new Object(); 
    
    public V getOrCreate(Key k, Supplier<V> valueCreator) {
        V cachedItem = cache.get(k);
        if(cachedItem == null) {
            synchronized (cacheModificationLock) {
                if(cachedItem == null) {
                    cachedItem = valueCreator.get();
                    cache.put(k, cachedItem);
                }
            }
        }
        return cachedItem;
    }

    @Override
    public V get(Key k) {
        return cache.get(k);
    }

    @Override
    public void remove(Key k) {
        synchronized (cacheModificationLock) {
            cache.remove(k);
        }
    }

    @Override
    public void remove(V value) {
        if(value == null) {
            return;
        }
        
        for (Entry<Key, V> cacheEntry : cache.entrySet()) {
            if(value.equals(cacheEntry.getValue())) {
                Key keyToDelete = cacheEntry.getKey();
                remove(keyToDelete);
                return;
            }
        }
    }
    
    @Override
    public boolean containsKey(Key k) {
        return cache.containsKey(k);
    }

}
