package com.jogl.rubik.util.cache.impl;

import java.util.Arrays;

import com.jogl.rubik.util.cache.Key;

public class GeneralKey implements Key {
    
    protected Object[] keys;
    
    public GeneralKey(Object... keys) {
        this.keys = keys;
    }

    public Object[] getKeys() {
        return keys;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(keys);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GeneralKey other = (GeneralKey) obj;
        if (!Arrays.equals(keys, other.keys))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "GeneralKey [keys=" + Arrays.toString(keys) + "]";
    }

}
