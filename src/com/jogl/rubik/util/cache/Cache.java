package com.jogl.rubik.util.cache;

import java.util.function.Supplier;

public interface Cache<K extends Key, V> {
    V getOrCreate(K k, Supplier<V> valueCreator);
    V get(K k);
    boolean containsKey(K k);
    void remove(K k);
    void remove(V v);
}
