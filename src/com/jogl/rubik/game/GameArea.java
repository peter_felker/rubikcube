package com.jogl.rubik.game;

import javax.media.opengl.DebugGL3;
import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilitiesImmutable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLException;
import javax.media.opengl.awt.GLCanvas;

import com.jogamp.opengl.math.FloatUtil;
import com.jogamp.opengl.math.Matrix4;
import com.jogamp.opengl.math.Ray;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogl.rubik.cube.RubikCube;
import com.jogl.rubik.math.camera.Camera;
import com.jogl.rubik.math.color.Color;
import com.jogl.rubik.math.matrix.MatrixContainer;
import com.jogl.rubik.math.rotation.Angles;
import com.jogl.rubik.math.vector.Vector4f;

public class GameArea extends GLCanvas implements GLEventListener {
    private static final long serialVersionUID = 1L;
    
    public static final int FPS = 120;
    public static final float ZNEAR = 1.0f;
    public static final float ZFAR = 20.0f;
    private static Color BACKGROUND = new Color(0.392f, 0.584f, 0.929f);
    
    private RubikCube rubikCube;
    private MatrixContainer matrixContainer;
    private Camera camera;
    private FPSAnimator animator;
    
    public GameArea(GLCapabilitiesImmutable glCapabilities, RubikCube rubikCube) throws GLException {
        super(glCapabilities);
        this.rubikCube = rubikCube;
        
        initGLDependencies();
        initCamera();
    }

    private void initGLDependencies() {
        animator = new FPSAnimator(this, FPS);
        addGLEventListener(this);
    }

    private void initCamera() {
        Vector4f eye = new Vector4f(0.0f, 0.0f, 5.0f, 1.0f);
        Vector4f center = new Vector4f(0.0f, 0.0f, -1.0f, 1.0f);
        Vector4f up = new Vector4f(0.0f, 1.0f, 0.0f, 0.0f);
        camera = new Camera(eye, center, up);
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL3 gl = drawable.getGL().getGL3();
        drawable.setGL(new DebugGL3(gl));
        
        enableGLEffects(gl);
        gl.glClearColor(BACKGROUND.getRed(), BACKGROUND.getGreen(),
                        BACKGROUND.getBlue(), BACKGROUND.getAlpha());
        
        initMatrixContainer();

        rubikCube.init(drawable);
        animator.start();
    }

    private void initMatrixContainer() {
        matrixContainer = new MatrixContainer();
        matrixContainer.setView(camera);
        updatePerspectiveMatrix();
    }

    private void enableGLEffects(GL3 gl) {
        gl.glEnable(GL3.GL_DEPTH_TEST);
        gl.glEnable(GL3.GL_LINE_SMOOTH);
        gl.glEnable(GL3.GL_POLYGON_SMOOTH);
        gl.glHint(GL3.GL_POLYGON_SMOOTH_HINT, GL3.GL_NICEST);
        gl.glDepthFunc(GL3.GL_LEQUAL);
        
        // Only show the triangles (no filling takes place).
        //gl.glPolygonMode(GL3.GL_FRONT_AND_BACK, GL3.GL_LINE);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL3 gl = drawable.getGL().getGL3();
        gl.glClear(GL3.GL_COLOR_BUFFER_BIT | GL3.GL_DEPTH_BUFFER_BIT);
        
        matrixContainer.initModelStack();
        
        rubikCube.display(drawable, matrixContainer);
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        animator.stop();
        rubikCube.dispose(drawable);
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL3 gl = drawable.getGL().getGL3();
        gl.glViewport(0, 0, width, height);
        updatePerspectiveMatrix();
    }
    
    private void updatePerspectiveMatrix() {
        float fieldOfView = Angles.DEGREE_45.getRadian();
        float aspect = (float)getWidth()/(float)getHeight();
        matrixContainer.setPerspective(fieldOfView, aspect, ZNEAR, ZFAR);
    }

    /**
     * Calculates a Ray by the given mouse screen coordinates.
     * @param mouseX mouse screen X coord.
     * @param mouseY mouse screen Y coord.
     * @return The calculated Ray object.
     */
    public Ray calcRay(float mouseX, float mouseY) {
        // We copy the view and perspective matrices
        // because these are going to be inverted in the process.
        Matrix4 viewM = matrixContainer.copyView();
        Matrix4 persM = matrixContainer.copyPerspective();
        
        int[] viewport = getViewPort();
        
        Ray ray = new Ray();
        FloatUtil.mapWinToRay(mouseX, viewport[3]-mouseY, 0.0f, 1.0f,
                              viewM.getMatrix(), 0, persM.getMatrix(), 0,
                              viewport, 0, ray, new float[16], new float[16], new float[4]);
        return ray;
    }

    private int[] getViewPort() {
        int[] viewport = new int[4];
        
        viewport[0] = 0;
        viewport[1] = 0;
        viewport[2] = getWidth();
        viewport[3] = getHeight();
        
        return viewport;
    }

    /**
     * Zooms in or out if the new distance falls into the ZNEAR-ZFAR range.
     * @param amount
     */
    public void zoom(float amount) {
        boolean isEyeUpdated = camera.updateEyeZ(amount, ZNEAR, ZFAR);
        
        if(isEyeUpdated) {
            matrixContainer.setView(camera);
        }
    }
    
}
