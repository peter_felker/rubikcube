package com.jogl.rubik.game;

import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;

import com.jogamp.opengl.math.Ray;
import com.jogl.rubik.cube.RubikCube;
import com.jogl.rubik.cube.tile.Tile;
import com.jogl.rubik.game.GameOptionsDialog.GameOptions;

/**
 * The purpose of this class is to handle to the mouse events in the gameArea.
 */
public class GameMouseListener implements MouseInputListener, MouseWheelListener {

    public RubikCube rubikCube;
    public GameArea gameArea;
    
    private int prevMouseY;
    private int prevMouseX;

    private Tile selectedTile1;
    private Tile selectedTile2;
    
    public GameMouseListener(GameArea gameArea, RubikCube rubikCube) {
        this.rubikCube = rubikCube;
        this.gameArea = gameArea;
    }
    
    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if(SwingUtilities.isLeftMouseButton(mouseEvent)) {
            selectedTile1 = traceTile(mouseEvent);
        } else if(SwingUtilities.isMiddleMouseButton(mouseEvent) || SwingUtilities.isRightMouseButton(mouseEvent)) {
            prevMouseX = mouseEvent.getX();
            prevMouseY = mouseEvent.getY();
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        if(SwingUtilities.isLeftMouseButton(mouseEvent)) {
            selectedTile2 = traceTile(mouseEvent);
            rubikCube.rotateByTiles(selectedTile1, selectedTile2);
        }
    }
    
    /**
     * Traces a tile using the given mouse event (x,y coords).
     * @param mouseEvent
     * @return The tile if found, otherwise null.
     */
    private Tile traceTile(MouseEvent mouseEvent) {
        float mouseX = mouseEvent.getX();
        float mouseY = mouseEvent.getY();
        Ray ray = gameArea.calcRay(mouseX, mouseY);
        Tile tile = rubikCube.getClosestTile(ray);
        return tile;
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        float mouseSensitivity = GameOptions.getMouseSensitivity();
        
        if(SwingUtilities.isMiddleMouseButton(mouseEvent)) {
            rotateCubeByXY(mouseEvent, mouseSensitivity);
        } else if(SwingUtilities.isRightMouseButton(mouseEvent)) {
            rotateCubeByZ(mouseEvent, mouseSensitivity);
        }
    }

    private void rotateCubeByXY(MouseEvent mouseEvent, float mouseSensitivity) {
        int actY = mouseEvent.getY();
        int actX = mouseEvent.getX();
        float rotX = calcRotationAmount(actY, prevMouseY, mouseSensitivity);
        float rotY = calcRotationAmount(actX, prevMouseX, mouseSensitivity);

        rubikCube.rotateCube(rotX, rotY);

        prevMouseX = actX;
        prevMouseY = actY;
    }
    
    private void rotateCubeByZ(MouseEvent mouseEvent, float mouseSensitivity) {
        int actY = mouseEvent.getY();
        float rotZ = calcRotationAmount(prevMouseY, actY, mouseSensitivity);

        rubikCube.rotateCubeZ(rotZ);

        prevMouseY = actY;
    }
    
    private float calcRotationAmount(int p1, int p2, float mouseSensitivity) {
        return (float)Math.toRadians(p1-p2)*mouseSensitivity;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mouseEvent) {
        // Zoom in or out if the wheel is not pushed down.
        if(!SwingUtilities.isMiddleMouseButton(mouseEvent)) {
            float amount = mouseEvent.getWheelRotation();
            gameArea.zoom(amount);
        }
    }
    
    
    
    @Override
    public void mouseEntered(MouseEvent mouseEvent) { }

    @Override
    public void mouseExited(MouseEvent mouseEvent) { }
    
    @Override
    public void mouseClicked(MouseEvent mouseEvent) { }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) { }

}
