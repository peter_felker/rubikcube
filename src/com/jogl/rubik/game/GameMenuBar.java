package com.jogl.rubik.game;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

import com.jogl.rubik.util.window.WindowUtils;

public class GameMenuBar extends JMenuBar implements ActionListener {
    private static final long serialVersionUID = 1L;

    private static final String GAME = "Game";
    private static final String OPTIONS = "Options";
    private static final String NEW_GAME = "New game";
    private static final String SHUFFLE = "Shuffle";
    
    private static final String HELP = "Help";
    private static final String HOW_TO_PLAY = "How to play";
    private static final String ABOUT = "About";
    
    private GameWindow gameWindow;
    
    public GameMenuBar(GameWindow gameWindow) {
        this.gameWindow = gameWindow;
        
        createGameMenu();
        createHelpMenu();
    }

    private void createGameMenu() {
        JMenu gameMenu = addMenu(GAME);
        addMenuItem(gameMenu, SHUFFLE);
        addMenuItem(gameMenu, NEW_GAME);
        addSeparator(gameMenu);
        addMenuItem(gameMenu, OPTIONS);
    }
    
    private void createHelpMenu() {
        JMenu helpMenu = addMenu(HELP);
        addMenuItem(helpMenu, HOW_TO_PLAY);
        addMenuItem(helpMenu, ABOUT);
    }

    /**
     * Adds a menu to this menu bar.
     * @param menuText
     * @return The created menu bar.
     */
    private JMenu addMenu(String menuText) {
        JMenu menu = new JMenu(menuText);
        add(menu);
        return menu;
    }
    
    /**
     * Adds a menu item to the given menu, and the menu item's action command.
     * @param menu The new menu item will be added to this menu.
     * @param menuItemText This will be the menu item's text and the action command.
     */
    private void addMenuItem(JMenu menu, String menuItemText) {
        JMenuItem menuItem = new JMenuItem(menuItemText);
        menuItem.setActionCommand(menuItemText);
        menuItem.addActionListener(this);
        menu.add(menuItem);
    }
    
    
    private void addSeparator(JMenu menu) {
        menu.add(new JSeparator());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCmd = e.getActionCommand();
        
        switch(actionCmd) {
            case OPTIONS:
                openGameOptionsDialog();
            break;
            case NEW_GAME:
                startNewGameIfNeeded();
            break;
            case SHUFFLE:
                shuffleRubikCube();
            break;
            case HOW_TO_PLAY:
                openHelp();
            break;
            case ABOUT:
                openAbout();
            break;
        }
    }

    private void openGameOptionsDialog() {
        GameOptionsDialog gameOptionsDialog = new GameOptionsDialog();
        WindowUtils.moveToScreenCenter(gameOptionsDialog);
        gameOptionsDialog.setVisible(true);
    }
    
    private void startNewGameIfNeeded() {
        int answer = JOptionPane.showConfirmDialog(this,
                        "Are you sure you want to start a new game?\n"
                        + "All progress in this game will be lost!",
                        "Start new game?", JOptionPane.YES_NO_OPTION);
        if(answer == JOptionPane.YES_OPTION) { 
            gameWindow.restartGame();
        }
    }
    
    private void shuffleRubikCube() {
        gameWindow.rubikCube.shuffle();
    }
    
    private void openHelp() {
        showInformationFromFile(HOW_TO_PLAY, "HELP.md");
    }

    private void openAbout() {
        showInformationFromFile(ABOUT, "LICENSE.md");
    }
    
    private void showInformationFromFile(String title, String pathToFile) {
        List<String> infoTexts = getTextFromFile(pathToFile);
        
        if(infoTexts != null) {
            showInformation(title, infoTexts);
        }
    }
    
    private List<String> getTextFromFile(String pathToFile) {
        try {
            return Files.readAllLines(Paths.get(pathToFile));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private void showInformation(String title, List<String> infoTexts) {
        String infoText =
                    infoTexts
                       .stream()
                       .reduce((line1, line2) -> line1 + "\n" + line2)
                       .get();
        
        showInformation("About", infoText);
    }
    
    private void showInformation(String title, String infoText) {
        JOptionPane.showMessageDialog(this, infoText, title, JOptionPane.INFORMATION_MESSAGE);
    }

}
