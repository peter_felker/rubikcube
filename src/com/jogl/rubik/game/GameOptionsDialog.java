package com.jogl.rubik.game;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class GameOptionsDialog extends JDialog implements ActionListener {
    private static final long serialVersionUID = 1L;

    private static final String APPLY = "Apply";
    private static final int MAX_CUBE_SIZE = 200;
    
    private int gridX;
    private int gridY;
    
    private JSlider gameSpeedSlider;
    private JSlider mouseSensitivitySlider;
    private JSpinner cubeSizeSpinner;
    
    public GameOptionsDialog() {
        setLayout(new GridBagLayout());
        setWindowProperties();
        
        gridY = 0;
        addOptionFields();
        addApplyButton();

        pack();
    }

    private void setWindowProperties() {
        setTitle("Options");
        setResizable(false);
        setModal(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    private void addOptionFields() {
        addGameSpeedOptionField();
        addMouseSensitivityOptionField();
        addCubeSizeOptionField();
    }

    private void addGameSpeedOptionField() {
        int gameSpeed = Math.round(GameOptions.getGameSpeed());
        gameSpeedSlider = new JSlider(JSlider.HORIZONTAL,  1, GameArea.FPS, gameSpeed);
        addOptionField("Game speed: ", gameSpeedSlider);
    }
    
    private void addMouseSensitivityOptionField() {
        int mouseSensitivity = Math.round(GameOptions.getMouseSensitivity());
        mouseSensitivitySlider = new JSlider(JSlider.HORIZONTAL, 1, 10, mouseSensitivity);
        addOptionField("Mouse sensitivity: ", mouseSensitivitySlider);
    }
    
    private void addCubeSizeOptionField() {
        int cubeSize = GameOptions.getCubeSize();
        SpinnerModel spinnerModel = new SpinnerNumberModel(cubeSize, 2, MAX_CUBE_SIZE, 1);
        cubeSizeSpinner = new JSpinner(spinnerModel);
        String toolTipText = "Min value=2, Max value="+MAX_CUBE_SIZE+"; " +
                             "Need to start a new game in order to take effect!";
        addOptionField("Cube size: ", cubeSizeSpinner, toolTipText);
    }
    
    /**
     * Same as addOptionField(String labelText, JComponent component, String toolTip)
     * but without the toolTip.
     */
    private void addOptionField(String labelText, JComponent component) {
        addOptionField(labelText, component, null);
    }
    
    /**
     * Adds a new row to the GameOptions, which contains:
     * 1. A description label
     * 2. A JComponent for modifying a certain game option.
     * 3. A toolTip text.
     * @param labelText
     * @param component
     * @param toolTip
     */
    private void addOptionField(String labelText, JComponent component, String toolTip) {
        gridX = 0;
        GridBagConstraints gridBagConstraint = createCommonGridBagConstraint();
        JLabel label = new JLabel(labelText);
        label.setToolTipText(toolTip);
        add(label, gridBagConstraint);
        
        gridX = 1;
        gridBagConstraint = createCommonGridBagConstraint();
        component.setToolTipText(toolTip);
        add(component, gridBagConstraint);
        
        ++gridY;
    }
    
    private GridBagConstraints createCommonGridBagConstraint() {
        GridBagConstraints gridBagConstraint = new GridBagConstraints();
        gridBagConstraint.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraint.insets = new Insets(3, 3, 3, 3);
        gridBagConstraint.gridx = gridX;
        gridBagConstraint.gridy = gridY;
        return gridBagConstraint;
    }
    
    private void addApplyButton() {
        gridX = 0;
        GridBagConstraints gridBagConstraint = createCommonGridBagConstraint();
        gridBagConstraint.gridwidth = 2;
        
        addButton(APPLY, gridBagConstraint);
        
        ++gridY;
    }

    private void addButton(String textAndActionCommand, GridBagConstraints gridBagConstraint) {
        JButton applyButton = new JButton(textAndActionCommand);
        applyButton.setActionCommand(textAndActionCommand);
        applyButton.addActionListener(this);
        add(applyButton, gridBagConstraint);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCmd = e.getActionCommand();
        switch (actionCmd) {
            case APPLY:
                applyChangesAndDispose();
            break;
        }
    }

    private void applyChangesAndDispose() {
        revalidateOptionFields();
        updateGameOptions();
        dispose();
    }

    private void revalidateOptionFields() {
        cubeSizeSpinner.revalidate();
        gameSpeedSlider.revalidate();
        mouseSensitivitySlider.revalidate();
    }
    
    private void updateGameOptions() {
        float newGameSpeed = gameSpeedSlider.getValue();
        GameOptions.setGameSpeed(newGameSpeed);
        
        float newMouseSensitivity = mouseSensitivitySlider.getValue();
        GameOptions.setMouseSensitivity(newMouseSensitivity);

        int newCubeSize = (int) cubeSizeSpinner.getValue();
        GameOptions.setCubeSize(newCubeSize);
    }
    
    /**
     * GameOptions can be modified only by GameOptionsDialog (setters are private) and
     * any other class can read these options (getters are public).
     */
    public static class GameOptions {
        private GameOptions() { }

        public static int getCubeSize() {
            Preferences prefs = getPreferences();
            return prefs.getInt("CubeSize", 3);
        }
        
        private static void setCubeSize(int cubeSize) {
            Preferences prefs = getPreferences();
            prefs.putInt("CubeSize", cubeSize);
        }

        public static float getMouseSensitivity() {
            Preferences prefs = getPreferences();
            return prefs.getFloat("MouseSensitivity", 1.0f);
        }
        
        private static void setMouseSensitivity(float mouseSensitivity) {
            Preferences prefs = getPreferences();
            prefs.putFloat("MouseSensitivity", mouseSensitivity);
        }
        
        public static float getGameSpeed() {
            Preferences prefs = getPreferences();
            return prefs.getFloat("GameSpeed", 1.0f);
        }
        
        private static void setGameSpeed(float gameSpeed) {
            Preferences prefs = getPreferences();
            prefs.putFloat("GameSpeed", gameSpeed);
        }

        private static Preferences getPreferences() {
            return Preferences.userRoot();
        }
    }

}
