package com.jogl.rubik.game;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URL;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import com.jogl.rubik.cube.RubikCube;
import com.jogl.rubik.game.GameOptionsDialog.GameOptions;

public class GameWindow extends JFrame implements WindowListener {
    private static final long serialVersionUID = 1L;

    RubikCube rubikCube;
    GameArea gameArea;
    GameMouseListener gameMouseListener;
    GameMenuBar menuBar;

    public GameWindow() {
        setLookAndFeel();
        setWindowProperties();
        addEventListeners();
        createGameMenuBar();
    }

    private void setWindowProperties() {
        setTitle("Rubik's Cube");
        setSize(800, 600);
        setIcon("/images/rubiks_cube.png");
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }
    
    private void addEventListeners() {
        addWindowListener(this);
    }

    public void createGameMenuBar() {
        menuBar = new GameMenuBar(this);
        setJMenuBar(menuBar);
    }
    
    public void setIcon(String iconPath) {
        URL url = GameWindow.class.getResource(iconPath);
        ImageIcon imageIcon = new ImageIcon(url);
        setIconImage(imageIcon.getImage());
    }

    public void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void restartGame() {
        clearGameArea();
        initGameArea();
    }

    private void clearGameArea() {
        gameArea.disposeGLEventListener(gameArea, true);
        getContentPane().remove(gameArea);
    }
    
    public void initGameArea() {
        initRubikCube();

        GLCapabilities glCapabilities = getGLCapabilities();
        gameArea = new GameArea(glCapabilities, rubikCube);
        gameMouseListener = new GameMouseListener(gameArea, rubikCube);
        gameArea.addMouseMotionListener(gameMouseListener);
        gameArea.addMouseListener(gameMouseListener);
        gameArea.addMouseWheelListener(gameMouseListener);

        getContentPane().add(gameArea);
        revalidate();
    }

    private void initRubikCube() {
        int cubeSize = GameOptions.getCubeSize();
        rubikCube = new RubikCube(cubeSize);
    }

    private GLCapabilities getGLCapabilities() {
        GLProfile profile = GLProfile.get(GLProfile.GL3);
        GLCapabilities capabilities = new GLCapabilities(profile);
        return capabilities;
    }

    @Override
    public void windowClosing(WindowEvent arg0) {
        int answer = JOptionPane.showConfirmDialog(this,
                "Are you sure you want to exit?\n"
                + "All progress in this game will be lost!",
                "Are you sure?", JOptionPane.YES_NO_OPTION);
        if(answer == JOptionPane.YES_OPTION) { 
            this.dispose();
        }
    }
    
    
    @Override
    public void windowActivated(WindowEvent arg0) { }

    @Override
    public void windowDeactivated(WindowEvent arg0) { }

    @Override
    public void windowDeiconified(WindowEvent arg0) { }

    @Override
    public void windowIconified(WindowEvent arg0) { }

    @Override
    public void windowOpened(WindowEvent arg0) { }

    @Override
    public void windowClosed(WindowEvent e) { }
}
