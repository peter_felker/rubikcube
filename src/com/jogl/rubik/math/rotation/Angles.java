package com.jogl.rubik.math.rotation;

public enum Angles {

    DEGREE_0(0.0f),
    DEGREE_45(0.7853981633974483f),
    DEGREE_90(1.570796326794897f),
    DEGREE_135(2.356194490192345f),
    DEGREE_180((float)Math.PI),
    DEGREE_225(3.926990816987241f),
    DEGREE_270(4.71238898038469f),
    DEGREE_315(5.497787143782138f),
    DEGREE_360(6.283185307179586f);
    
    float rad;
    
    Angles(float rad) {
        this.rad = rad;
    }
    
    public float getRadian() {
        return rad;
    }
}
