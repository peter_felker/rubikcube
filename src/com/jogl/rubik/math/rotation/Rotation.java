package com.jogl.rubik.math.rotation;

import com.jogamp.opengl.math.Matrix4;
import com.jogamp.opengl.math.Quaternion;

public class Rotation {

    Axis axis;
    float radian;
    
    public Rotation(Axis axis, float radian) {
        super();
        this.axis = axis;
        this.radian = radian;
    }

    public Rotation(Rotation rotation) {
        this(rotation.getAxis(), rotation.getRadian());
    }

    public Axis getAxis() {
        return axis;
    }

    public float getRadian() {
        return radian;
    }

    public void setAxis(Axis axis) {
        this.axis = axis;
    }

    public void setRadian(float radian) {
        this.radian = radian;
    }
    
    public void apply(Matrix4 outMatrix) {
        float radian = getRadian();
        float x = getAxis().asArray()[0];
        float y = getAxis().asArray()[1];
        float z = getAxis().asArray()[2];

        outMatrix.rotate(radian, x, y, z);
    }

    public Quaternion asQuaternion() {
        Quaternion result = new Quaternion();
        result.setFromAngleNormalAxis(radian, axis.asArray());
        return result;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((axis == null) ? 0 : axis.hashCode());
        result = prime * result + Float.floatToIntBits(radian);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Rotation other = (Rotation) obj;
        if (axis != other.axis)
            return false;
        if (Float.floatToIntBits(radian) != Float.floatToIntBits(other.radian))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Rotation [axis=" + axis + ", radian=" + radian + "]";
    }

}
