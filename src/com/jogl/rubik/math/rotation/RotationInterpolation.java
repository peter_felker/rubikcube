package com.jogl.rubik.math.rotation;

import com.jogamp.opengl.math.Quaternion;

public class RotationInterpolation {
    
    private Rotation rotation;
    private float amount;
    private boolean inProgress;

    private Quaternion startOrientation;
    private Quaternion endOrientation;
    private Quaternion interpolationQuaternion;

    public RotationInterpolation() {
        amount = 1.0f;
        inProgress = false;
    }
    
    public RotationInterpolation(Axis axis, float radian) {
        this(new Rotation(axis, radian));
    }
    
    public RotationInterpolation(Rotation rotation) {
        super();
        resetInterpolation(rotation);
    }
    
    public void resetInterpolation(Axis axis, float radian) {
        resetInterpolation(new Rotation(axis, radian));
    }
    
    public void resetInterpolation(Rotation rotation) {
        this.rotation = rotation;
        
        startOrientation = new Quaternion();
        startOrientation.setIdentity();
        endOrientation = new Quaternion();
        endOrientation.setFromAngleNormalAxis(rotation.radian, rotation.axis.asArray());
        
        resetInterpolation();
    }

    public void resetInterpolation() {
        interpolationQuaternion =  new Quaternion();
        setAmount(0.0f);
        inProgress = true;
    }
    
    public Quaternion addAmount(float amountToAdd) {
        float newAmount = amount + amountToAdd;
        return setAmount(newAmount);
    }
    
    public Quaternion setAmount(float amount) {
        if(amount >= 0.0f && amount <= 1.0f) {
            this.amount = amount;
        } else if(amount > 1.0f) {
            this.amount = 1.0f;
        } else if(amount < 0.0f) {
            this.amount = 0.0f;
        }

        return makeInterpolationStep();
    }
    
    public boolean hasReachedEnd() {
        return amount == 1.0f;
    }
    
    private Quaternion makeInterpolationStep() {
        interpolationQuaternion.setSlerp(startOrientation, endOrientation, amount);
        return getInterpolationQuaternion();
    }

    public float getAmount() {
        return amount;
    }
    
    public Rotation getRotation() {
        return rotation;
    }

    public Quaternion getInterpolationQuaternion() {
        return new Quaternion(interpolationQuaternion);
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(amount);
        result = prime * result + (inProgress ? 1231 : 1237);
        result = prime * result
                + ((rotation == null) ? 0 : rotation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RotationInterpolation other = (RotationInterpolation) obj;
        if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
            return false;
        if (inProgress != other.inProgress)
            return false;
        if (rotation == null) {
            if (other.rotation != null)
                return false;
        } else if (!rotation.equals(other.rotation))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "RotationInterpolation [rotation=" + rotation + ", amount="
                + amount + ", inProgress=" + inProgress + "]";
    }

}
