package com.jogl.rubik.math.rotation;

import com.jogl.rubik.math.vector.Vector4f;

public enum Axis {
    X(new Vector4f(1.0f, 0.0f, 0.0f, 0.0f)),
    Y(new Vector4f(0.0f, 1.0f, 0.0f, 0.0f)),
    Z(new Vector4f(0.0f, 0.0f, 1.0f, 0.0f));
    
    Vector4f vec;

    Axis(Vector4f vec) {
        this.vec = vec;
    }
    
    public float[] asArray() {
        return vec.asArray();
    }
    
    public Vector4f getVector() {
        return vec;
    }

}
