package com.jogl.rubik.math.camera;

import com.jogl.rubik.math.vector.Vector4f;

/**
 * Camera object, represented by eye, center and up vectors.
 */
public class Camera {

    private Vector4f eye;
    private Vector4f center;
    private Vector4f up;

    public Camera() { }
    
    public Camera(Vector4f eye, Vector4f center, Vector4f up) {
        super();
        this.eye = eye;
        this.center = center;
        this.up = up;
    }
    
    /**
     * Updates the eye's Z position if the new eye's Z position falls into the given min and max values.
     * With this we can control how far is the camera from the cube (zoom-in, zoom-out).
     * @param amount
     * @param min The new eye's Z value cannot be less than this parameter.
     * @param max The new eye's Z value cannot exceed this parameter.
     * @return The setting was successful.
     */
    public boolean updateEyeZ(float amount, float min, float max) {
        float newDistance = eye.getZ() + amount;
        
        if(newDistance > min && newDistance < max) {
            eye = eye.setZ(newDistance);
            return true;
        }
        
        return false;
    }
    
    public Vector4f getEye() {
        return eye;
    }

    public void setEye(Vector4f eye) {
        this.eye = eye;
    }

    public Vector4f getCenter() {
        return center;
    }

    public void setCenter(Vector4f center) {
        this.center = center;
    }

    public Vector4f getUp() {
        return up;
    }

    public void setUp(Vector4f up) {
        this.up = up;
    }
    
}
