package com.jogl.rubik.math.matrix;

import java.util.Stack;

import com.jogamp.opengl.math.Matrix4;

public class MatrixStack extends Stack<Matrix4> {
    private static final long serialVersionUID = 1L;
    
    @Override
    public Matrix4 peek() {
        Matrix4 mat = super.peek();
        Matrix4 copiedMat = new Matrix4();
        copiedMat.multMatrix(mat);
        return copiedMat;
    }

}
