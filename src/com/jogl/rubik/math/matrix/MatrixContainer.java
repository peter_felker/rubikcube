package com.jogl.rubik.math.matrix;

import com.jogamp.opengl.math.FloatUtil;
import com.jogamp.opengl.math.Matrix4;
import com.jogl.rubik.math.camera.Camera;
import com.jogl.rubik.math.vector.Vector4f;

public class MatrixContainer {

    private MatrixStack modelMatrixStack;
    private Matrix4 viewMatrix;
    private Matrix4 perspectiveMatrix;
    
    public MatrixContainer() {
        modelMatrixStack = new MatrixStack();
        viewMatrix = new Matrix4();
        perspectiveMatrix = new Matrix4();
    }

    public MatrixStack getModelStack() {
        return modelMatrixStack;
    }

    public Matrix4 getView() {
        return viewMatrix;
    }

    public Matrix4 copyView() {
        Matrix4 copyView = new Matrix4();
        copyView.multMatrix(viewMatrix);
        return copyView;
    }

    public void setView(float[] eye, float[] center, float[] up) {
        viewMatrix.loadIdentity();
        FloatUtil.makeLookAt(viewMatrix.getMatrix(), 0, eye, 0, center, 0, up, 0, new float[16]);
    }
    
    public void setView(Vector4f eye, Vector4f center, Vector4f up) {
        setView(eye.asArray(), center.asArray(), up.asArray());
    }
    
    public void setView(Camera camera) {
        setView(camera.getEye(), camera.getCenter(), camera.getUp());
    }
    
    public void setView(Matrix4 view) {
        this.viewMatrix = view;
    }

    public Matrix4 getPerspective() {
        return perspectiveMatrix;
    }

    public Matrix4 copyPerspective() {
        Matrix4 copyPerspective = new Matrix4();
        copyPerspective.multMatrix(perspectiveMatrix);
        return copyPerspective;
    }
    
    public void setPerspective(float fovyRad, float aspect, float zNear, float zFar) {
        perspectiveMatrix.loadIdentity();
        perspectiveMatrix.makePerspective(fovyRad, aspect, zNear, zFar);
    }
    
    public void setPerspective(Matrix4 perspective) {
        this.perspectiveMatrix = perspective;
    }

    public void initModelStack() {
        modelMatrixStack.clear();
        Matrix4 modelMatrix = new Matrix4();
        modelMatrixStack.push(modelMatrix);
    }

}
