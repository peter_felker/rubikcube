package com.jogl.rubik.math.vector;

import com.jogamp.opengl.math.Matrix4;
import com.jogl.rubik.math.rotation.Rotation;

public final class Vector3i extends AbstractVector<Vector3i> implements Cloneable {

    public Vector3i() {
        super(3);
    }
    
    public Vector3i(int x, float y, float z) {
        super(x, y, z);
    }
    
    public Vector3i(int... elements) {
        this(elements[0], elements[1], elements[2]);
    }
    
    public Vector3i(float... elements) {
        this(Math.round(elements[0]), Math.round(elements[1]), Math.round(elements[2]));
    }
    
    public Vector3i(Vector4f vect4) {
        this(vect4.getX(), vect4.getY(), vect4.getZ());
    }
    
    public Vector3i(Vector3i vect3) {
        this(vect3.vector);
    }

    public Vector3i rotate(Rotation rotation) {
        Vector4f vect4 = new Vector4f(this);
        vect4 = vect4.rotate(rotation);
        return new Vector3i(vect4);
    }

    public Vector3i mul(Matrix4 mat) {
        Vector4f vect4 = new Vector4f(this);
        vect4 = vect4.mul(mat);
        return new Vector3i(vect4);
    }

    public Vector3i set(int index, int value) {
        Vector3i copiedVector = new Vector3i(this);
        copiedVector.vector[index] = value;
        return copiedVector;
    }
    
    public Vector3i setX(int value) {
        return set(0, value);
    }
    public int getX() {
        return (int) vector[0];
    }

    public Vector3i setY(int value) {
        return set(1, value);
    }
    public int getY() {
        return (int) vector[1];
    }

    public Vector3i setZ(int value) {
        return set(2, value);
    }
    public int getZ() {
        return (int) vector[2];
    }
    
    public int get(int index) {
        return (int)vector[index];
    }

    @Override
    protected Vector3i createCopy(float[] elems) {
        return new Vector3i(elems);
    }

    @Override
    public Vector3i clone() {
        return createCopy(asArray());
    }
    
    @Override
    public String toString() {
        return "[x=" + getX() + ",y=" + getY() + ",z=" + getZ();
    }

}
