package com.jogl.rubik.math.vector;

import java.util.Arrays;

public abstract class AbstractVector<T extends AbstractVector<T>> implements Cloneable {
    protected final float[] vector;
    
    public AbstractVector(int size) {
        vector = new float[size];
    }
    
    public AbstractVector(float... vector) {
        this.vector = vector;
    }

    public AbstractVector(T otherVect) {
        this.vector = otherVect.asArray();
    }
    
    public long getSize() {
        return vector.length;
    }
    
    public float[] asArray() {
        return Arrays.copyOf(vector, vector.length);
    }

    public T roundAllFields() {
        float[] result = new float[vector.length];
        for (int i = 0; i < vector.length; i++) {
            result[i] = Math.round(vector[i]);
        }
        return createCopy(result);
    }
    
    public T mul(float scalar) {
        float[] result = new float[vector.length];
        for (int i = 0; i < vector.length; i++) {
            result[i] = vector[i]*scalar;
        }
        return createCopy(result);
    }

    public T add(T otherVector) {
        float[] result = clone().asArray();
        float[] otherVect = otherVector.asArray();
        for (int i = 0; i < result.length; i++) {
            result[i] += otherVect[i];
        }
        return createCopy(result);
    }

    public T sub(T otherVector) {
        float[] result = clone().asArray();
        float[] otherVect = otherVector.asArray();
        for (int i = 0; i < result.length; i++) {
            result[i] -= otherVect[i];
        }
        return createCopy(result);
    }
    
    public T replaceNonZeroWith(float replacement) {
        float[] result = clone().asArray();
        for (int i = 0; i < result.length; i++) {
            if(result[i] != 0.0f) {
                result[i] = replacement;
            }
        }
        return createCopy(result);
    }
    
    public float distance(T otherVector) {
        float[] otherVect = otherVector.asArray();
        
        float sum = 0.0f;
        for (int i = 0; i < vector.length; i++) {
            float diff = otherVect[i] - vector[i];
            sum += Math.pow(diff, 2.0f);
        }
        
        float distance = (float) Math.sqrt(sum);
        
        return distance;
    }

    public T normalize() {
        float length = 0.0f;
        for (int i = 0; i < vector.length; i++) {
            length += vector[i]*vector[i];
        }
        length = (float) Math.sqrt(length);
        
        float[] result = new float[vector.length];
        for (int i = 0; i < vector.length; i++) {
            result[i] = vector[i]/length;
        }
        
        return createCopy(result);
    }
    
    public long size() {
        return vector.length;
    }

    @Override
    public abstract T clone();
    
    protected abstract T createCopy(float[] elems);

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(vector);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractVector<?> other = (AbstractVector<?>) obj;
        if (!Arrays.equals(vector, other.vector))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "vector=" + Arrays.toString(vector);
    }
}
