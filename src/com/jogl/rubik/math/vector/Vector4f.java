package com.jogl.rubik.math.vector;

import com.jogamp.opengl.math.Matrix4;
import com.jogl.rubik.math.rotation.Rotation;

public final class Vector4f extends AbstractVector<Vector4f> implements Cloneable {

    public Vector4f() {
        super(4);
    }
    
    public Vector4f(float x, float y, float z, float w) {
        super(x, y, z, w);
    }
    
    public Vector4f(float[] elements) {
        this(elements[0], elements[1], elements[2], elements[3]);
    }
    
    public Vector4f(Vector3i vect3) {
        this(vect3.getX(), vect3.getY(), vect3.getZ(), 1.0f);
    }
    
    public Vector4f rotate(Rotation rotation) {
        Matrix4 rotMatrix = new Matrix4();
        rotation.apply(rotMatrix);
        return mul(rotMatrix);
    }

    public Vector4f mul(Matrix4 mat) {
        float[] result = new float[4];
        mat.multVec(vector, result);
        return new Vector4f(result);
    }

    public Vector4f set(int index, float value) {
        Vector4f copiedVector = new Vector4f(vector);
        copiedVector.vector[index] = value;
        return copiedVector;
    }
    
    public Vector4f setX(float value) {
        return set(0, value);
    }
    public float getX() {
        return vector[0];
    }

    public Vector4f setY(float value) {
        return set(1, value);
    }
    public float getY() {
        return vector[1];
    }

    public Vector4f setZ(float value) {
        return set(2, value);
    }
    public float getZ() {
        return vector[2];
    }

    public Vector4f setW(float value) {
        return set(3, value);
    }
    public float getW() {
        return vector[3];
    }
    
    public float get(int index) {
        return vector[index];
    }
    
    @Override
    protected Vector4f createCopy(float[] elems) {
        return new Vector4f(elems);
    }

    @Override
    public Vector4f clone() {
        return createCopy(asArray());
    }
    
    @Override
    public String toString() {
        return "[x=" + getX() + ",y=" + getY() + ",z=" + getZ() + ",w=" + getW() + "]";
    }

}
