package com.jogl.rubik.math.ray;

import com.jogamp.opengl.math.Matrix4;
import com.jogamp.opengl.math.Ray;
import com.jogl.rubik.math.vector.Vector4f;

public class OBBox {

    private Vector4f lowXYZ;
    private Vector4f highXYZ;

    public OBBox(Vector4f lowXYZ, Vector4f highXYZ) {
        super();
        this.lowXYZ = lowXYZ;
        this.highXYZ = highXYZ;
    }
    
    public void mult(Matrix4 modelMatrix) {
        lowXYZ = lowXYZ.mul(modelMatrix);
        highXYZ = highXYZ.mul(modelMatrix);
    }
    
    public float intersectsRay(Ray r) {
        // r.dir is unit direction vector of ray
        float dirfracX = 1.0f / r.dir[0];
        float dirfracY = 1.0f / r.dir[1];
        float dirfracZ = 1.0f / r.dir[2];
        // lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
        // r.org is origin of ray
        float t1 = (lowXYZ.getX() - r.orig[0])*dirfracX;
        float t2 = (highXYZ.getX() - r.orig[0])*dirfracX;
        float t3 = (lowXYZ.getY() - r.orig[1])*dirfracY;
        float t4 = (highXYZ.getY() - r.orig[1])*dirfracY;
        float t5 = (lowXYZ.getZ() - r.orig[2])*dirfracZ;
        float t6 = (highXYZ.getZ() - r.orig[2])*dirfracZ;
    
        float tmin = Math.max(Math.max(Math.min(t1, t2), Math.min(t3, t4)), Math.min(t5, t6));
        float tmax = Math.min(Math.min(Math.max(t1, t2), Math.max(t3, t4)), Math.max(t5, t6));
    
        // if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behind us
        if (tmax < 0) {
            return 0.0f;
        }
    
        // if tmin > tmax, ray doesn't intersect AABB
        if (tmin > tmax) {
            return 0.0f;
        }

        return tmin;
    }
    
    public Vector4f getLowXYZ() {
        return lowXYZ;
    }
    public Vector4f getHighXYZ() {
        return highXYZ;
    }
    public void setLowXYZ(Vector4f lowXYZ) {
        this.lowXYZ = lowXYZ;
    }
    public void setHighXYZ(Vector4f highXYZ) {
        this.highXYZ = highXYZ;
    }    
    
    @Override
    public String toString() {
        return "OBBox [lowXYZ=" + lowXYZ + ", highXYZ=" + highXYZ /*
                + ", centerXYZ=" + centerXYZ */ + "]";
    }
}
