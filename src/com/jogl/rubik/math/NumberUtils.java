package com.jogl.rubik.math;

import java.util.Random;

public class NumberUtils {

    private NumberUtils() {}
    
    public static boolean isEven(float num) {
        return isEven(Math.round(num));
    }
    
    public static boolean isEven(int num) {
        return ((num & 1) == 0);
    }
    
    public static boolean equals(float a, float b, float epsilon) {
        return Math.abs(a-b) < epsilon;
    }
    
    /**
     * Returns a psuedo-random number between min and max, inclusive.
     * 
     * @param min
     * @param max
     * @return
     */
    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
    
}
