package com.jogl.rubik.math.color;

import java.util.Arrays;

public class Color {

    private float[] colorCode;
    
    public Color(float red, float green, float blue) {
        this(red, green, blue, 1.0f);
    }
    
    public Color(float red, float green, float blue, float alpha) {
        colorCode = new float[4];
        setRed(red);
        setGreen(green);
        setBlue(blue);
        setAlpha(alpha);
    }
    
    public float getRed() {
        return colorCode[0];
    }
    public void setRed(float red) {
        colorCode[0] = red;
    }
    
    public float getGreen() {
        return colorCode[1];
    }
    public void setGreen(float green) {
        colorCode[1] = green;
    }
    
    public float getBlue() {
        return colorCode[2];
    }
    public void setBlue(float blue) {
        colorCode[2] = blue;
    }
    
    public float getAlpha() {
        return colorCode[3];
    }
    public void setAlpha(float alpha) {
        colorCode[3] = alpha;
    }
    
    public float[] asArray() {
        return copyColorCode(colorCode.length);
    }
    
    public float[] asArrayWithoutAlpha() {
        return copyColorCode(colorCode.length-1);
    }
    
    private float[] copyColorCode(int newLength) {
        return Arrays.copyOf(colorCode, newLength);
    }
    
}
