package com.jogl.rubik.render;

import javax.media.opengl.GLAutoDrawable;

import com.jogl.rubik.math.matrix.MatrixContainer;

public interface Renderable {

    public void init(GLAutoDrawable drawable);
    public void display(GLAutoDrawable drawable, MatrixContainer matrixContainer);
    public void dispose(GLAutoDrawable drawable);
    
}
