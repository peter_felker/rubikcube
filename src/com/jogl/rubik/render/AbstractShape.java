package com.jogl.rubik.render;

import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;

import com.jogamp.opengl.math.Matrix4;
import com.jogl.rubik.math.matrix.MatrixContainer;
import com.jogl.rubik.render.buffer.VertexArrayObject;
import com.jogl.rubik.render.buffer.VertexBufferObject;
import com.jogl.rubik.render.buffer.VertexIndexArray;
import com.jogl.rubik.render.buffer.VertexBufferInfo;
import com.jogl.rubik.render.buffer.VertexBufferInfoKey;
import com.jogl.rubik.render.shader.ShaderProgram;
import com.jogl.rubik.render.shader.ShaderResource;
import com.jogl.rubik.util.cache.Cache;
import com.jogl.rubik.util.cache.Key;
import com.jogl.rubik.util.cache.impl.GeneralCache;
import com.jogl.rubik.util.cache.impl.GeneralKey;

public abstract class AbstractShape implements Renderable {

    protected ShaderProgram shaderProg;
    protected VertexBufferInfo vertexInfo;

    private static Cache<Key, ShaderProgram> shaderProgramCache = new GeneralCache<>();
    private static Cache<Key, VertexBufferInfo> vertexCache = new GeneralCache<>();
    
    private boolean init;
    
    @Override
    public void init(GLAutoDrawable drawable) {
        GL3 gl = drawable.getGL().getGL3();

        List<String> shaderFiles = getShaderFiles();
        List<ShaderResource> shaderResources = new LinkedList<>();
        for (String shaderFile : shaderFiles) {
            shaderResources.add(new ShaderResource(getClass(),shaderFile));
        }
        Key shaderProgramKey = new GeneralKey(shaderResources);
        shaderProg = shaderProgramCache.getOrCreate(shaderProgramKey, () -> new ShaderProgram(shaderResources) );
        shaderProg.compile(gl);

        Key vertexInfoKey = createVertexInfoKey();
        vertexInfo = vertexCache.getOrCreate(vertexInfoKey, () -> createVertexInfo(gl));
        
        init = true;
    }

    private VertexBufferInfo createVertexInfo(GL3 gl) {
        int[] vertexIndices = getVertexIndices();
        float[] vertices = getVertices();
        float[] colors = getColors();
        
        VertexArrayObject vao = new VertexArrayObject(gl);
        vao.use(gl);

        VertexIndexArray via = new VertexIndexArray(gl, vertexIndices.length);
        via.fill(gl, vertexIndices);

        VertexBufferObject vbo = new VertexBufferObject(gl, vertices.length+colors.length);
        vbo.fill(gl, vertices, colors);

        shaderProg.enableParameter(gl, "vPosition");
        shaderProg.enableParameter(gl, "vColor", vertices.length);

        vao.dontUse(gl);
        
        VertexBufferInfo vertexInfo = new VertexBufferInfo(vao, vbo, via);
        return vertexInfo;
    }
    
    @Override
    public void display(GLAutoDrawable drawable, MatrixContainer matrixContainer) {
        GL3 gl = drawable.getGL().getGL3();
        Matrix4 modelMatrix =  matrixContainer.getModelStack().peek();

        shaderProg.useShader(gl);
        vertexInfo.useVAO(gl);

        preDisplay(gl, modelMatrix);
        
        Matrix4 view = matrixContainer.getView();
        Matrix4 perspective = matrixContainer.getPerspective();
        setUniformParameters(gl, modelMatrix, view, perspective);

        int drawMode = getDrawMode();
        gl.glDrawElements(drawMode, getVertexIndices().length, GL3.GL_UNSIGNED_INT, 0);

        vertexInfo.dontUseVAO(gl);
        shaderProg.dontUseShader(gl);
        
        postDisplay(gl, modelMatrix);
    }

    private void setUniformParameters(GL3 gl, Matrix4 model, Matrix4 view, Matrix4 perspective) {
        //Matrix4 allTransMatrix = new Matrix4();
        //allTransMatrix.multMatrix(perspective);
        //allTransMatrix.multMatrix(view);
        //allTransMatrix.multMatrix(model);
        //shaderProg.setUniformParameter(gl, "mAll", allTransMatrix);
        shaderProg.setUniformParameter(gl, "mM", model);
        shaderProg.setUniformParameter(gl, "mV", view);
        shaderProg.setUniformParameter(gl, "mP", perspective);
    }

    protected void preDisplay(GL3 gl, Matrix4 modelMatrix) {
        
    }
    
    protected void postDisplay(GL3 gl, Matrix4 modelMatrix) {
        
    }
    
    @Override
    public void dispose(GLAutoDrawable drawable) {
        GL3 gl = drawable.getGL().getGL3();

        if(vertexInfo != null && !vertexInfo.isDisposed()) {
            vertexInfo.dispose(gl);
            vertexCache.remove(vertexInfo);
        }
        if(shaderProg != null && !shaderProg.isDisposed()) {
            shaderProg.dispose(gl);
            shaderProgramCache.remove(shaderProg);
        }
    }
    
    private Key createVertexInfoKey() {
        int[] vertexIndices = getVertexIndices();
        float[] vertices = getVertices();
        float[] colors = getColors();
        
        return new VertexBufferInfoKey(vertexIndices, vertices, colors);
    }
    
    public boolean isInit() {
        return init;
    }
    
    
    public abstract float[] getVertices();
    
    public abstract int[] getVertexIndices();
    
    public abstract float[] getColors();
    
    public abstract List<String> getShaderFiles();
    
    public abstract int getDrawMode();

}
