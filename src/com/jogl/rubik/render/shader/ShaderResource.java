package com.jogl.rubik.render.shader;

import javax.media.opengl.GL3;

public class ShaderResource {

    private Class<?> clazz;
    private String fileName;
    private int shaderType;
    
    public ShaderResource(Class<?> clazz, String fileName) {
        super();
        this.clazz = clazz;
        this.fileName = fileName;
        
        String ext = getFileExtension(fileName);
        if("vsh".equalsIgnoreCase(ext)) {
            shaderType = GL3.GL_VERTEX_SHADER;
        } else if("fsh".equalsIgnoreCase(ext)) {
            shaderType = GL3.GL_FRAGMENT_SHADER;
        } else {
            throw new IllegalArgumentException("Shader type not supported: " + ext);
        }
    }
    
    private String getFileExtension(String pathToFile) {
        return pathToFile.substring(pathToFile.lastIndexOf('.')+1);
    }
    
    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getShaderType() {
        return shaderType;
    }

    public void setShaderType(int shaderType) {
        this.shaderType = shaderType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
        result = prime * result
                + ((fileName == null) ? 0 : fileName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShaderResource other = (ShaderResource) obj;
        if (clazz == null) {
            if (other.clazz != null)
                return false;
        } else if (!clazz.equals(other.clazz))
            return false;
        if (fileName == null) {
            if (other.fileName != null)
                return false;
        } else if (!fileName.equals(other.fileName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ShaderResource [clazz=" + clazz + ", fileName=" + fileName + "]";
    }
    
}
