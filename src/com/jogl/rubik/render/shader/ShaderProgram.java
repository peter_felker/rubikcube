package com.jogl.rubik.render.shader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL3;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.math.Matrix4;

public class ShaderProgram {
    private int shaderProgramId;
    
    private List<ShaderResource> shaderResources;
    private List<Integer> shaderIds;
    
    private boolean compiled = false;
    private boolean disposed = false;
    
    public ShaderProgram(List<ShaderResource> shaderResources) {
        this.shaderResources = shaderResources;
    }
    
    public void compile(GL3 gl) {
        if(compiled) {
            return;
        }
        
        shaderProgramId = gl.glCreateProgram();
        shaderIds = new ArrayList<>();
        for (ShaderResource shaderResource : shaderResources) {
            int shaderId = gl.glCreateShader(shaderResource.getShaderType());
            String[] shaderSource = loadShaderSource(shaderResource.getClazz(), shaderResource.getFileName());
            gl.glShaderSource(shaderId, 1, shaderSource, null, 0);
            gl.glCompileShader(shaderId);
            gl.glAttachShader(shaderProgramId, shaderId);
            shaderIds.add(shaderId);
        }
    
        gl.glLinkProgram(shaderProgramId);
        errorCheck(gl);
        compiled = true;
    }

    private void errorCheck(GL3 gl) {
        gl.glValidateProgram(shaderProgramId);
        IntBuffer intBuffer = IntBuffer.allocate(1);
        gl.glGetProgramiv(shaderProgramId, GL3.GL_LINK_STATUS, intBuffer);
        
        if (intBuffer.get(0) != 1) {
            gl.glGetProgramiv(shaderProgramId, GL3.GL_INFO_LOG_LENGTH, intBuffer);
            int size = intBuffer.get(0);
            System.err.println("Program link error: ");
            if (size > 0) {
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetProgramInfoLog(shaderProgramId, size, intBuffer, byteBuffer);
                for (byte b : byteBuffer.array()) {
                    System.err.print((char) b);
                }
            } else {
                System.out.println("Unknown");
            }
            System.exit(1);
        }
    }
     
    public String[] loadShaderSource(Class<?> clazz, String fileName) {
        StringBuilder sb = new StringBuilder();
        try {
            InputStream is = clazz.getResourceAsStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Shader is " + sb.toString());
        return new String[]{ sb.toString() };
    }
    
    public void dispose(GL3 gl) {
        for (Integer shaderId : shaderIds) {
            gl.glDetachShader(shaderProgramId, shaderId);
            gl.glDeleteShader(shaderId);
        }
        gl.glDeleteProgram(shaderProgramId);
        disposed = true;
    }
    
    public int useShader(GL3 gl) {
        gl.glUseProgram(shaderProgramId);
        return shaderProgramId;
    }
    
    public int getAttribLocation(GL3 gl, String attribName) {
        return gl.glGetAttribLocation(shaderProgramId, attribName);
    }
    
    public int getUniformLocation(GL3 gl, String uniformName) {
        return gl.glGetUniformLocation(shaderProgramId, uniformName);
    }
    
    public void enableParameter(GL3 gl, String paramName) {
        enableParameter(gl, paramName, 0L);
    }
    
    public void enableParameter(GL3 gl, String paramName, long offset) {
        int paramId = getAttribLocation(gl, paramName);
        gl.glEnableVertexAttribArray(paramId);
        gl.glVertexAttribPointer(paramId, 3, GL3.GL_FLOAT, false, 0, offset*Buffers.SIZEOF_FLOAT);
    }
    
    public void setUniformParameter(GL3 gl, String uniformParameterName, Matrix4 matrix) {
        int paramLocation = getUniformLocation(gl, uniformParameterName);
        FloatBuffer matrixBuff = FloatBuffer.wrap(matrix.getMatrix());
        gl.glUniformMatrix4fv(paramLocation, 1, false, matrixBuff);
    }
     
    public void dontUseShader( GL3 gl ) {
        gl.glUseProgram(0);
    }

    public boolean isCompiled() {
        return compiled;
    }
    
    public boolean isDisposed() {
        return disposed;
    }
    
    public List<ShaderResource> getShaderResources() {
        return shaderResources;
    }

    public int getId() {
        return shaderProgramId;
    }

}