package com.jogl.rubik.render.buffer;

import java.nio.FloatBuffer;

import javax.media.opengl.GL3;

import com.jogamp.common.nio.Buffers;

public class VertexBufferObject extends AbstractVertexObject {

    public VertexBufferObject(GL3 gl, int size) {
        super(gl, size);
    }
    
    public void fill(GL3 gl, float[] vertices, float[] colors) {
        fill(gl, FloatBuffer.wrap(vertices), FloatBuffer.wrap(colors));
    }
    
    public void fill(GL3 gl, FloatBuffer verticesBuff, FloatBuffer colorsBuff) {
        gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, id);

        gl.glBufferData(GL3.GL_ARRAY_BUFFER, getSizeInBytes(),
                        null, GL3.GL_STATIC_DRAW);
        gl.glBufferSubData(GL3.GL_ARRAY_BUFFER, 0,
                           verticesBuff.limit()*Buffers.SIZEOF_FLOAT, verticesBuff);
        gl.glBufferSubData(GL3.GL_ARRAY_BUFFER, verticesBuff.limit()*Buffers.SIZEOF_FLOAT,
                           colorsBuff.limit()*Buffers.SIZEOF_FLOAT, colorsBuff); 
    }

    public int getSizeInBytes() {
        return size*Buffers.SIZEOF_FLOAT;
    }

}
