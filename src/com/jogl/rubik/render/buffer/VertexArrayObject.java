package com.jogl.rubik.render.buffer;

import java.nio.IntBuffer;

import javax.media.opengl.GL3;

public class VertexArrayObject {

    private int vaoId;
    
    public VertexArrayObject(GL3 gl) {
        IntBuffer vertexArray = IntBuffer.allocate(1);
        gl.glGenVertexArrays(1, vertexArray);
        vaoId = vertexArray.get(0);
    }
    
    public void use(GL3 gl) {
        gl.glBindVertexArray(vaoId);
    }
    
    public void dontUse(GL3 gl) {
        gl.glBindVertexArray(0);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + vaoId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VertexArrayObject other = (VertexArrayObject) obj;
        if (vaoId != other.vaoId)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "VertexArrayObject [vaoId=" + vaoId + "]";
    }
    
}
