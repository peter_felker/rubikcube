package com.jogl.rubik.render.buffer;

import java.util.Arrays;

import com.jogl.rubik.util.cache.Key;

public class VertexBufferInfoKey implements Key {

    int[] vertexIndices;
    float[] vertices;
    float[] colors;
    
    public VertexBufferInfoKey(int[] vertexIndices, float[] vertices, float[] colors) {
        super();
        this.vertexIndices = vertexIndices;
        this.vertices = vertices;
        this.colors = colors;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(colors);
        result = prime * result + Arrays.hashCode(vertexIndices);
        result = prime * result + Arrays.hashCode(vertices);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VertexBufferInfoKey other = (VertexBufferInfoKey) obj;
        if (!Arrays.equals(colors, other.colors))
            return false;
        if (!Arrays.equals(vertexIndices, other.vertexIndices))
            return false;
        if (!Arrays.equals(vertices, other.vertices))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "VertexArrayInfo [vertexIndices="
                + Arrays.toString(vertexIndices) + ", vertices="
                + Arrays.toString(vertices) + ", colors="
                + Arrays.toString(colors) + "]";
    }
    
}
