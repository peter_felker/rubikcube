package com.jogl.rubik.render.buffer;

import javax.media.opengl.GL3;

public class VertexBufferInfo {

    private VertexArrayObject vao;
    private VertexBufferObject vbo;
    private VertexIndexArray via;
    
    private boolean disposed;
    
    public VertexBufferInfo(VertexArrayObject vao, VertexBufferObject vbo, VertexIndexArray via) {
        super();
        this.vao = vao;
        this.vbo = vbo;
        this.via = via;
        
        disposed = false;
    }
    
    public void useVAO(GL3 gl) {
        vao.use(gl);
    }
    
    public void dontUseVAO(GL3 gl) {
        vao.dontUse(gl);
    }
    
    public void dispose(GL3 gl) {
        disposeIfNecessary(gl, via);
        disposeIfNecessary(gl, vbo);
        disposed = true;
    }
    
    private void disposeIfNecessary(GL3 gl, AbstractVertexObject vertexObject) {
        if(vertexObject != null && !vertexObject.isDisposed()) { 
            vertexObject.dispose(gl);
        }
    }
    
    public boolean isDisposed() {
        return disposed;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((vao == null) ? 0 : vao.hashCode());
        result = prime * result + ((vbo == null) ? 0 : vbo.hashCode());
        result = prime * result + ((via == null) ? 0 : via.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VertexBufferInfo other = (VertexBufferInfo) obj;
        if (vao == null) {
            if (other.vao != null)
                return false;
        } else if (!vao.equals(other.vao))
            return false;
        if (vbo == null) {
            if (other.vbo != null)
                return false;
        } else if (!vbo.equals(other.vbo))
            return false;
        if (via == null) {
            if (other.via != null)
                return false;
        } else if (!via.equals(other.via))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "VertexBufferInfo [vao=" + vao +
               ", vbo=" + vbo + ", via=" + via +
               ", disposed=" + disposed + "]";
    }

}
