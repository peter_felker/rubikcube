package com.jogl.rubik.render.buffer;

import java.nio.IntBuffer;

import javax.media.opengl.GL3;

public abstract class AbstractVertexObject {

    protected int id;
    protected int size;
    protected boolean disposed;
    protected boolean init;
    
    public AbstractVertexObject(GL3 gl, int size) {
        this.size = size;
        IntBuffer buffer = IntBuffer.allocate(size);
        gl.glGenBuffers(1, buffer);
        this.id = buffer.get();
        this.init = true;
    }
    
    public void dispose(GL3 gl) {
        if(!disposed) {
            IntBuffer buffToDelete = IntBuffer.wrap(new int[] {id});
            gl.glDeleteBuffers(1, buffToDelete);
            disposed = true;
        }
    }

    public abstract int getSizeInBytes();
    
    public int getId() {
        return id;
    }
    
    public int getSize() {
        return size;
    }
    
    public boolean isDisposed() {
        return disposed;
    }

    public boolean isInit() {
        return init;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractVertexObject other = (AbstractVertexObject) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return getClass() + " [id=" + id + ", size=" + size
                + ", disposed=" + disposed + ", init=" + init + "]";
    }


}
