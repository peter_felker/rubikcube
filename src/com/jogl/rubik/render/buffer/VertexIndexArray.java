package com.jogl.rubik.render.buffer;

import java.nio.IntBuffer;

import javax.media.opengl.GL3;

import com.jogamp.common.nio.Buffers;

public class VertexIndexArray extends AbstractVertexObject {

    public VertexIndexArray(GL3 gl, int size) {
        super(gl, size);
    }
    
    public void fill(GL3 gl, int[] vertexIndiciesArr) {
        fill(gl, IntBuffer.wrap(vertexIndiciesArr));
    }
    
    public void fill(GL3 gl, IntBuffer vertexIndicesBuff) {
        gl.glBindBuffer(GL3.GL_ELEMENT_ARRAY_BUFFER, id);
        gl.glBufferData(GL3.GL_ELEMENT_ARRAY_BUFFER, getSizeInBytes(), vertexIndicesBuff, GL3.GL_STATIC_DRAW);
    }

    @Override
    public int getSizeInBytes() {
        return size*Buffers.SIZEOF_INT;
    }
    
}
