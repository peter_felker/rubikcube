package com.jogl.rubik.cube;

import com.jogl.rubik.cube.tile.Tile;
import com.jogl.rubik.cube.tile.TileIndex;
import com.jogl.rubik.cube.tile.TileRotationType;
import com.jogl.rubik.math.rotation.Axis;
import com.jogl.rubik.math.rotation.Rotation;
import com.jogl.rubik.math.vector.Vector3i;
import com.jogl.rubik.math.vector.Vector4f;

public enum CubeSide {
    FRONT(Axis.Y, TileRotationType.NO_ROTATION.getRadian()),
    RIGHT(Axis.Y, TileRotationType.COUNTER_CLOCKWISE.getRadian()),
    BACK(Axis.Y, 2*TileRotationType.COUNTER_CLOCKWISE.getRadian()),
    LEFT(Axis.Y, TileRotationType.CLOCKWISE.getRadian()),
    TOP(Axis.X, TileRotationType.CLOCKWISE.getRadian()),
    BOTTOM(Axis.X, TileRotationType.COUNTER_CLOCKWISE.getRadian());

    private Rotation sideRotation;
    
    CubeSide(Axis axis, float radian) {
        sideRotation = new Rotation(axis, radian);
    }
    
    /**
     * Returns a rotation for this side.
     * This rotation can be handy for calculating {@link TileIndex}s.
     * @return
     */
    public Rotation getSideRotation() {
        return sideRotation;
    }
    
    /**
     * Returns the side of the given tile.
     * @param tile
     * @return
     * 
     * @see {@link TileIndex}
     */
    public static CubeSide getSideFor(Tile tile) {
        return getSideFor(tile.getIndex());
    }
    
    /**
     * Returns the side of the given tile's index.
     * @param index
     * @return
     * 
     * @see {@link TileIndex}
     */
    public static CubeSide getSideFor(TileIndex index) {
        return getSideFor(index.asVector());
    }
    
    public static CubeSide getSideFor(Vector4f vector) {
        return getSideFor(new Vector3i(vector));
    }
    
    /**
     * Returns the side of the given vector.
     * @param vector
     * @return
     * 
     * @see {@link TileIndex}
     */
    public static CubeSide getSideFor(Vector3i vector) {
        int x = vector.getX();
        int y = vector.getY();
        int z = vector.getZ();
        
        if(z == 1) {
            return FRONT;
        } else if(z == -1) {
            return BACK;
        } else if(y == 1) {
            return TOP;
        } else if(y == -1) {
            return BOTTOM;
        } else if(x == 1) {
            return RIGHT;
        } else if(x == -1) {
            return LEFT;
        }
        
        throw new IllegalArgumentException("No side for " + vector + "!");
    }

}
