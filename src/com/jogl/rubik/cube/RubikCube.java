package com.jogl.rubik.cube;

import java.util.HashSet;
import java.util.Set;

import javax.media.opengl.GLAutoDrawable;

import com.jogamp.opengl.math.Matrix4;
import com.jogamp.opengl.math.Quaternion;
import com.jogamp.opengl.math.Ray;
import com.jogl.rubik.cube.tile.IndexStep;
import com.jogl.rubik.cube.tile.Tile;
import com.jogl.rubik.cube.tile.TileColor;
import com.jogl.rubik.cube.tile.TileContainer;
import com.jogl.rubik.cube.tile.TileIndex;
import com.jogl.rubik.cube.tile.TileIndexRange;
import com.jogl.rubik.cube.tile.TileRotationEndListener;
import com.jogl.rubik.math.NumberUtils;
import com.jogl.rubik.math.matrix.MatrixContainer;
import com.jogl.rubik.math.matrix.MatrixStack;
import com.jogl.rubik.math.rotation.Axis;
import com.jogl.rubik.math.rotation.Rotation;
import com.jogl.rubik.render.Renderable;
import com.jogl.rubik.util.collection.CollectionUtil;

/**
 * For more information about implementation details
 * see {@link Tile} and {@link TileIndex}.
 */
public class RubikCube implements Renderable, TileRotationEndListener {
    
    private int size;
    private TileContainer tiles;
    private Set<Tile> tilesToRotate;

    private Quaternion cubeRotationQuaternion;
    private int shuffleCount;
    
    public RubikCube(int size) {
        this.size = size;
        tiles = new TileContainer(size);
        cubeRotationQuaternion = new Quaternion();

        createTiles();
    }

    private void createTiles() {
        TileIndexRange indexRange = new TileIndexRange(size);
        
        for (CubeSide cubeSide : CubeSide.values()) {
            for (Integer x : indexRange) {
                for (Integer y : indexRange) {
                    createTile(cubeSide, x, y);
                }
            }
        }
    }
    
    public void createTile(CubeSide cubeSide, int x, int y) {
        TileIndex index = new TileIndex(cubeSide, x, y);
        TileColor color = TileColor.getColorForSide(cubeSide);
        Tile tile = new Tile(size, index, color);
        tile.addRotationEndListener(this);
        
        tiles.put(index, tile);
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        tiles.foreach(tile -> {
            if(tile != null) {
                tile.init(drawable);
            }
        });
    }
    
    @Override
    public void display(GLAutoDrawable drawable, MatrixContainer matrixContainer) {
        MatrixStack modelMatrixStack = matrixContainer.getModelStack();
        Matrix4 modelMatrix = modelMatrixStack.peek();

        modelMatrix.rotate(cubeRotationQuaternion);
        modelMatrixStack.push(modelMatrix);

        tiles.foreach(tile -> tile.display(drawable,matrixContainer));    

        modelMatrixStack.pop();
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        tiles.foreach(tile -> {
            if(tile != null && tile.isInit()) {
                tile.dispose(drawable);
            }
        });
    }

    /**
     * The function waits for every tile to be rotated
     * and when the last tile is done with the rotation
     * then modifies the rotated tiles' index.
     * 
     * @see {@link TileIndex}
     */
    @Override
    public void tileRotationEnded(Tile tileToRotate) {
        if(!isTilesRotationOn()) {
            for (Tile tile : tilesToRotate) {
                Rotation latestTileRotation = tile.getLatestRotation();
                TileIndex index = tile.getIndex();
                TileIndex newIndex = index.rotate(latestTileRotation);

                tiles.put(newIndex, tile);
                tile.setIndex(newIndex);
            }

            tilesToRotate.clear();
            shuffle(shuffleCount-1);
        }
    }

    /**
     * @return True if there is any tile rotation in progress, otherwise false.
     */
    public boolean isTilesRotationOn() {        
        return tiles.foreachPredicate(tile -> tile.isRotationOn());
    }
    
    /**
     * Shuffles the Rubik's Cube by a random N times.
     */
    public void shuffle() {
        int shuffleCount = NumberUtils.randInt(7*size/2, 7*size);
        shuffle(shuffleCount);
    }
    
    /**
     * Shuffles the Rubik's Cube by the given times.
     * @param times
     */
    public void shuffle(int times) {
        shuffleCount = times;
        if(shuffleCount > 0) {
            Tile randomTile1 = getRandomTile();
            Tile randomTile2 = getRandomTileFromSameSide(randomTile1);
            rotateByTiles(randomTile1, randomTile2);
        } else {
            shuffleCount = 0;
        }
    }
    
    private Tile getRandomTile() {
        Tile randomTile = CollectionUtil.getRandomElem(tiles.values());
        return randomTile;
    }
    
    /**
     * Gets a random tile from the same side (and same row or column)
     * as the given randomTile1.
     * @param randomTile1
     * @return
     */
    private Tile getRandomTileFromSameSide(Tile randomTile1) {
        Set<Tile> tilesToChooseFrom = new HashSet<>();
        
        Set<IndexStep> possibleSteps = randomTile1.getPossibleSteps();
        for (IndexStep possibleStep : possibleSteps) {
            Set<Tile> collected = tiles.getTilesFromSameSide(randomTile1, possibleStep);
            tilesToChooseFrom.addAll(collected);
        }
        tilesToChooseFrom.remove(randomTile1);

        Tile randomTile2 = CollectionUtil.getRandomElem(tilesToChooseFrom);
        
        return randomTile2;
    }
    
    /**
     * Tries to rotate tiles along the given 2 tiles.
     * @param tile1
     * @param tile2
     */
    public void rotateByTiles(Tile tile1, Tile tile2) {
        if(tile1 == null || tile2 == null || isTilesRotationOn()) {
            return;
        }

        IndexStep indexStep = tile1.calcIndexStep(tile2);
        if(indexStep.isValid()) {
            Rotation rotation = indexStep.calcRotation(tile1);
            tilesToRotate =
                    tiles.getTilesToRotate(tile1, indexStep, rotation);
            tilesToRotate.forEach(tile -> tile.startRotation(rotation));
        }
    }
    
    public void rotateCube(float rotX, float rotY) {
        Quaternion q1 = new Quaternion();
        Quaternion q2 = new Quaternion();
        
        q1.setFromAngleNormalAxis(rotX, Axis.X.asArray());
        q2.setFromAngleNormalAxis(rotY, Axis.Y.asArray());

        cubeRotationQuaternion = q1.mult(q2).mult(cubeRotationQuaternion);
        cubeRotationQuaternion.normalize();
    }

    public void rotateCubeZ(float rotZ) {
        Quaternion q1 = new Quaternion();

        q1.setFromAngleNormalAxis(rotZ, Axis.Z.asArray());

        cubeRotationQuaternion = q1.mult(cubeRotationQuaternion);
        cubeRotationQuaternion.normalize();
    }
    
    public Tile getClosestTile(Ray ray) {
        return tiles.getClosestTile(ray);
    }

}
