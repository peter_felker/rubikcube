package com.jogl.rubik.cube.tile;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.jogamp.opengl.math.Ray;
import com.jogl.rubik.cube.CubeSide;
import com.jogl.rubik.math.ray.OBBox;
import com.jogl.rubik.math.rotation.Axis;
import com.jogl.rubik.math.rotation.Rotation;
import com.jogl.rubik.math.vector.Vector4f;

public class TileContainer extends HashMap<TileIndex, Tile> {
    private static final long serialVersionUID = 1L;

    private final TileIndexRange indexRange;
    
    public TileContainer(int cubeSize) {
        indexRange = new TileIndexRange(cubeSize);
    }
    
    public Set<Tile> getTilesToRotate(Tile tile, IndexStep indexStep) {
        Rotation rotation = indexStep.calcRotation(tile);
        return getTilesToRotate(tile, indexStep, rotation);
    }
    
    public Set<Tile> getTilesToRotate(Tile tile, IndexStep indexStep, Rotation rotation) {
        Set<Tile> tilesToRotate = new HashSet<>();
        tilesToRotate.add(tile);
        
        Set<Tile> tilesFromSameSide = getTilesFromSameSide(tile, indexStep);
        tilesToRotate.addAll(tilesFromSameSide);
        
        Set<Tile> tilesFromOtherSides = getTilesFromOtherSides(tilesToRotate, rotation);
        tilesToRotate.addAll(tilesFromOtherSides);
        
        CubeSide sideToRotate = whichSideNeedsRotation(tile, rotation.getAxis());
        if(sideToRotate != null) {
            Set<Tile> tilesFromSide = getTilesFromSide(sideToRotate);
            tilesToRotate.addAll(tilesFromSide);
        }
        
        return tilesToRotate;
    }
    
    private CubeSide whichSideNeedsRotation(Tile tile, Axis axis) {
        TileIndex index = tile.getIndex();
        Vector4f axisVect = axis.getVector();

        for (int i = 0; i < axisVect.size(); i++) {
            if(axisVect.get(i) != 0.0f) {
                if(index.get(i) == indexRange.getHighest()) {
                    return CubeSide.getSideFor(axisVect);
                } else if(index.get(i) == indexRange.getLowest()) {
                    axisVect = axisVect.mul(-1.0f);
                    return CubeSide.getSideFor(axisVect);
                }
            }
        }
        
        return null;
    }
    
    public Set<Tile> getTilesFromSide(CubeSide side) {
        Set<Tile> tilesFromSide = new HashSet<>();
        
        this.values()
            .stream()
            .filter(tile -> CubeSide.getSideFor(tile) == side)
            .forEach(tile -> tilesFromSide.add(tile));

        return tilesFromSide;
    }

    /**
     * Collects tiles from the same side as the given tile
     * by using the given indexStep.
     * <br/>
     * <b>Note:</b> This function won't add the given tile to the result set!
     * @param tile
     * @param indexStep
     * @return
     */
    public Set<Tile> getTilesFromSameSide(Tile tile, IndexStep indexStep) {
        TileIndex index = tile.getIndex();
        Set<TileIndex> indicesFromSameSide = new HashSet<>();

        Set<TileIndex> collectedIndices1 = index.collectIndices(indexStep, indexRange);
        indicesFromSameSide.addAll(collectedIndices1);
        
        IndexStep negatedDirection = indexStep.negateDirection();
        Set<TileIndex> collectedIndices2 = index.collectIndices(negatedDirection, indexRange);
        indicesFromSameSide.addAll(collectedIndices2);

        return getTiles(indicesFromSameSide);
    }

    /**
     * Collects tiles from other sides in circle
     * by using the given tiles
     * (every tile in tilesFromSameSide are from the same side)
     * and rotation.
     * <br/>
     * <b>Note:</b> This function won't add the given tilesFromSameSide to the result set!
     * @param tilesFromSameSide
     * @param rotation
     * @return
     */
    public Set<Tile> getTilesFromOtherSides(Set<Tile> tilesFromSameSide, Rotation rotation) {
        Set<TileIndex> indicesFromConnectingSides = new HashSet<>();
        
        int sides = 4;
        for (int i = 1; i < sides; i++) {
            Rotation rotationToUse = new Rotation(rotation.getAxis(), rotation.getRadian()*i);
            for (Tile tile : tilesFromSameSide) {
                TileIndex index = tile.getIndex();
                TileIndex rotatedIndex = index.rotate(rotationToUse);
                indicesFromConnectingSides.add(rotatedIndex);
            }
        }
        
        return getTiles(indicesFromConnectingSides);
    } 
    
    private Set<Tile> getTiles(Set<TileIndex> indices) {
        Set<Tile> tiles = new HashSet<>();

        for (TileIndex index : indices) {
            if(index.isValidIndex(indexRange)) {
                Tile tile = get(index);
                tiles.add(tile);
            }
        }
        
        return tiles;
    }
    
    public Tile getTile(CubeSide cubeSide, int x, int y) {
        TileIndex index = new TileIndex(cubeSide, x, y);
        return get(index);
    }
    
    public Tile getClosestTile(Ray ray) {
        float bestDistance = 0.0f;
        Tile selectedTile = null;
        for (Tile tile : values()) {
            OBBox tileBox = tile.getBoundingBox();
            float distance = tileBox.intersectsRay(ray);

            if(distance > 0.0f && (bestDistance == 0.0f || (distance < bestDistance))) {
                bestDistance = distance;
                selectedTile = tile;
            }
        }
        
        return selectedTile;
    }
    
    public void foreach(Consumer<Tile> consumer) {
        values().stream().forEach(consumer);
    }
    
    public boolean foreachPredicate(Predicate<Tile> predicate) {
        return values().parallelStream().anyMatch(predicate);
    }
    
}
