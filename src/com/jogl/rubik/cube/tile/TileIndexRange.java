package com.jogl.rubik.cube.tile;

import static com.jogl.rubik.math.NumberUtils.isEven;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TileIndexRange implements Iterable<Integer> {

    private List<Integer> range;
    
    /**
     * Generates an index range for the given cubeSize.
     * For instance for a 3x3x3 cube, the index range would be: [-2, 0, 2].
     * For a 4x4x4 cube the index range would be: [-4,-2, 2, 4].
     * @param cubeSize
     */
    public TileIndexRange(int cubeSize) {
        super();
        int highestIndexCoord;
        int lowestIndexCoord;
        
        if(isEven(cubeSize)) {
            highestIndexCoord = cubeSize;
        } else {
            highestIndexCoord = (int)Math.floor(cubeSize/2.0f)*2;
        }
        lowestIndexCoord = -highestIndexCoord;
        
        createRange(cubeSize, lowestIndexCoord, highestIndexCoord);
    }

    private void createRange(int cubeSize, int lowestIndexCoord, int highestIndexCoord) {
        range = new ArrayList<>();
        for (int i = lowestIndexCoord; i <= highestIndexCoord; i+=IndexStep.AMOUNT) {
            if(!skip(cubeSize, i)) {
                range.add(i);
            }
        }
    }
    
    public boolean isCoordWithinRange(Integer coord) {
        return coord >= getLowest() && coord <= getHighest();
    }
    
    /**
     * As mentioned in the {@link TileIndex} documentation:
     * We have to skip the 0 index coordinate if the Rubik's Cube's size is even,
     * because we want to have a symmetric structure.
     * @param cubeSize
     * @param index
     * @return
     */
    private boolean skip(int cubeSize, int indexCoord) {
        return isEven(cubeSize) && indexCoord == 0;
    }

    public boolean contains(int indexCoord) {
        return range.contains(indexCoord);
    }
    
    public int getLowest() {
        return range.get(0);
    }
    
    public int getHighest() {
        return range.get(range.size()-1);
    }
    
    @Override
    public Iterator<Integer> iterator() {
        return range.iterator();
    }

}
