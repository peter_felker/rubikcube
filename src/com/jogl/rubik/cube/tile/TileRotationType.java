package com.jogl.rubik.cube.tile;

import com.jogl.rubik.math.rotation.Angles;

/**
 * Contains the available rotation types for {@Tile}s.
 */
public enum TileRotationType {
    CLOCKWISE(-Angles.DEGREE_90.getRadian()),
    COUNTER_CLOCKWISE(Angles.DEGREE_90.getRadian()),
    NO_ROTATION(Angles.DEGREE_0.getRadian());
    
    private float radian;

    TileRotationType(float radian) {
        this.radian = radian;
    }
    
    public float getRadian() {
        return radian;
    }
    
}
