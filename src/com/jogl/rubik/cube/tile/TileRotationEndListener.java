package com.jogl.rubik.cube.tile;

public interface TileRotationEndListener {
    public void tileRotationEnded(Tile tile);
}
