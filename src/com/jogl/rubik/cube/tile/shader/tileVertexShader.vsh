#version 130

in vec4 vPosition;
in vec4 vColor;
out vec4 color;
uniform mat4 mM;
uniform mat4 mV;
uniform mat4 mP;
//uniform mat4 mAll;


void main () {
	color = vColor;
	gl_Position = mP*mV*mM*vPosition;
	//gl_Position = mAll*vPosition;
}