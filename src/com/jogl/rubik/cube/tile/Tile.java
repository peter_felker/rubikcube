package com.jogl.rubik.cube.tile;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.media.opengl.GL3;

import com.jogamp.opengl.math.Matrix4;
import com.jogamp.opengl.math.Quaternion;
import com.jogl.rubik.cube.CubeSide;
import com.jogl.rubik.game.GameArea;
import com.jogl.rubik.game.GameOptionsDialog.GameOptions;
import com.jogl.rubik.math.color.Color;
import com.jogl.rubik.math.ray.OBBox;
import com.jogl.rubik.math.rotation.Rotation;
import com.jogl.rubik.math.rotation.RotationInterpolation;
import com.jogl.rubik.math.vector.Vector4f;
import com.jogl.rubik.render.AbstractShape;
import com.jogl.rubik.render.Renderable;
import com.jogl.rubik.util.collection.CollectionUtil;

/**
 * The {@link RubikCube} consists of tiles rather than (sub)cubes.
 * The decision here was that it's much easier to work with tiles.
 * 
 * @see {@link TileIndex}
 */
public class Tile extends AbstractShape implements Renderable {

    public static final float VERTICIES[] = {
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,

        -0.9f, 0.9f, 0.02f, 
        -0.9f, -0.9f, 0.02f,
        0.9f, 0.9f, 0.02f,
        0.9f, -0.9f, 0.02f
    };
    
    public static final int VERTEX_INDICIES[] = {
        0,1,2,2,1,3,
        4,5,6,6,5,7
    };
    
    public static final Color BLANK_COLOR = new Color(0.0f, 0.0f, 0.0f);
    
    public static final float ROTATION_SPEED = TileRotationType.COUNTER_CLOCKWISE.getRadian()/GameArea.FPS;
    
    
    private TileColor tileColor;
    private TileIndex index;
    
    private int cubeSize;
    private float tileSize;
    
    private volatile Matrix4 latestModelMatrix;
    private RotationInterpolation tileRotation;
    private List<TileRotationEndListener> rotationEndListeners;
    
    
    public Tile(int cubeSize, TileIndex index, TileColor tileColor) {
        super();
        this.tileColor = tileColor;
        this.index = index;
        this.cubeSize = cubeSize;

        tileSize = 1.0f/cubeSize;
        tileRotation = new RotationInterpolation();
        rotationEndListeners = new ArrayList<>();
    }

    public OBBox getBoundingBox() {
        Vector4f boundingBoxLow = new Vector4f(-1.0f, -1.0f, 0.0f, 1.0f);
        Vector4f boundingBoxHigh = new Vector4f(1.0f, 1.0f, 0.02f, 1.0f);
        
        OBBox box = new OBBox(boundingBoxLow, boundingBoxHigh);
        box.mult(latestModelMatrix);
        
        return box;
    }

    @Override
    protected void preDisplay(GL3 gl, Matrix4 modelMatrix) {
        super.preDisplay(gl, modelMatrix);

        if(tileRotation.isInProgress()) {
            progressNextRotationPart(modelMatrix);
        }
        
        float worldX = index.calcWorldX(cubeSize);
        float worldY = index.calcWorldY(cubeSize);
        float worldZ = index.calcWorldZ(cubeSize);
        modelMatrix.translate(worldX, worldY, worldZ);
        modelMatrix.rotate(index.calcRotationQuaternion());
        modelMatrix.scale(tileSize, tileSize, 1.0f);

        latestModelMatrix = modelMatrix;
    }

    private void progressNextRotationPart(Matrix4 modelMatrix) {
        float gameSpeed = GameOptions.getGameSpeed();
        float rotationAmount = ROTATION_SPEED*gameSpeed;
        Quaternion tileRotationPart = tileRotation.addAmount(rotationAmount);
        modelMatrix.rotate(tileRotationPart);
    };
    
    @Override
    protected void postDisplay(GL3 gl, Matrix4 modelMatrix) {
        super.postDisplay(gl, modelMatrix);
        
        if(tileRotation.isInProgress() && tileRotation.hasReachedEnd()) {
            tileRotation.setInProgress(false);
            rotationEndListeners.forEach(listener -> listener.tileRotationEnded(this));
        }
    };

    @Override
    public float[] getVertices() {
        return VERTICIES;
    }
    
    @Override
    public int[] getVertexIndices() {
        return VERTEX_INDICIES;
    }

    @Override
    public float[] getColors() {
        List<Float> colors = new LinkedList<Float>();
        
        for (int i = 0; i < VERTICIES.length; i+=3) {
            Color colorToUse = isTileBack(i) ? BLANK_COLOR : tileColor.getColor();
            float[] colorFloatArr = colorToUse.asArrayWithoutAlpha();
            List<Float> colorFloatList = CollectionUtil.toFloatList(colorFloatArr);
            colors.addAll(colorFloatList);
        }
        
        return CollectionUtil.toFloatArray(colors);
    }

    private boolean isTileBack(int i) {
        return i < VERTICIES.length/2;
    }

    @Override
    public List<String> getShaderFiles() {
        List<String> shaderFiles = new ArrayList<>();
        
        shaderFiles.add("shader/tileVertexShader.vsh");
        shaderFiles.add("shader/tileFragmentShader.fsh");

        return shaderFiles;
    }
    
    @Override
    public int getDrawMode() {
        return GL3.GL_TRIANGLES;
    }
    
    public Set<IndexStep> getPossibleSteps() {
        return index.getPossibleSteps();
    }
    
    public IndexStep calcIndexStep(Tile tile2) {
        return index.calcIndexStep(tile2.getIndex());
    }

    public void startRotation(Rotation rotation) {
        tileRotation.resetInterpolation(rotation);
    }
    
    public void addRotationEndListener(TileRotationEndListener listener) {
        rotationEndListeners.add(listener);
    }
    
    public void removeRotationEndListener(TileRotationEndListener listener) {
        rotationEndListeners.remove(listener);
    }
    
    public boolean isRotationOn() {
        return tileRotation.isInProgress();
    }

    public Rotation getLatestRotation() {
        return tileRotation.getRotation();
    }
    
    public TileIndex getIndex() {
        return index;
    }

    public void setIndex(TileIndex index) {
        this.index = index;
    }

    public CubeSide getSide() {
        return CubeSide.getSideFor(this);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((index == null) ? 0 : index.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tile other = (Tile) obj;
        if (index == null) {
            if (other.index != null)
                return false;
        } else if (!index.equals(other.index))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Tile [color=" + tileColor + ", index=" + index + "]";
    }
    
}
