package com.jogl.rubik.cube.tile;

import com.jogl.rubik.cube.CubeSide;
import com.jogl.rubik.math.color.Color;

public enum TileColor {
    
    GREEN(new Color(0.0f, 0.9f, 0.0f)),
    RED(new Color(0.8f, 0.0f, 0.0f)),
    BLUE(new Color(0.0f, 0.0f, 0.9f)),
    ORANGE(new Color(1.0f, 0.5f, 0.0f)),
    WHITE(new Color(1.0f, 1.0f, 1.0f)),
    YELLOW(new Color(1.0f, 1.0f, 0.0f));

    private Color color;
    
    TileColor(Color color) {
        this.color = color;
    }
    
    public Color getColor() {
        return color;
    }
    
    public float[] asArray() {
        return color.asArray();
    }
    
    public float[] asArrayWithoutAlpha() {
        return color.asArrayWithoutAlpha();
    }
    
    public static TileColor getColorForSide(CubeSide cubeSide) {
        return values()[cubeSide.ordinal()];
    }

}
