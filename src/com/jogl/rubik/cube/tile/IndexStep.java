package com.jogl.rubik.cube.tile;

import com.jogl.rubik.cube.CubeSide;
import com.jogl.rubik.math.rotation.Axis;
import com.jogl.rubik.math.rotation.Rotation;
import com.jogl.rubik.math.vector.Vector3i;

/**
 * The class is meant to simplify the iteration between tiles.
 */
public final class IndexStep {

    public static final int AMOUNT = 2;
    
    final Vector3i direction;
    
    public IndexStep(TileIndex index1, TileIndex index2) {
        direction = prepareDirection(index1, index2);
    }
    
    private Vector3i prepareDirection(TileIndex index1, TileIndex index2) {
        Vector3i preparedDirection = index1.coords.sub(index2.coords);
        
        for (int i = 0; i < preparedDirection.size(); i++) {
            int directionCoord = preparedDirection.get(i);
            if(directionCoord != 0) {
                int step = (int)(Math.signum(directionCoord)*AMOUNT);
                preparedDirection = preparedDirection.set(i, step);
            }
        }
        
        return preparedDirection;
    }

    public IndexStep(Vector3i direction) {
        this.direction = direction;
    }
    
    /**
     * An IndexStep is valid if it's direction vector contains only
     * one element that is not 0.
     * @return
     */
    public boolean isValid() {
        int nonZeroElementCount = 0;
        for (int i = 0; i < direction.size(); i++) {
            if(direction.get(i) != 0) {
                ++nonZeroElementCount;
            }
        }
        
        return nonZeroElementCount == 1;
    }

    /**
     * Returns a copy of this IndexStep
     * in which the direction is negated (points to the other direction).
     * @return
     */
    public IndexStep negateDirection() {
        Vector3i copiedDirection = direction.mul(-1);
        return new IndexStep(copiedDirection);
    }
    
    public Rotation calcRotation(Tile tile) {
        return calcRotation(tile.getIndex());
    }
    
    public Rotation calcRotation(TileIndex index) {
        Axis axis = calcRotationAxis(index);
        TileRotationType rotType = calcRotationType(index);
        Rotation rotation = new Rotation(axis, rotType.getRadian());
        return rotation;
    }
    
    private Axis calcRotationAxis(TileIndex index) {
        boolean isX = isDirectionOrIndex(direction.getX(), index.getX());
        boolean isY = isDirectionOrIndex(direction.getY(), index.getY());
        boolean isZ = isDirectionOrIndex(direction.getZ(), index.getZ());
        
        if(isX && isY) {
            return Axis.Z;
        } else if(isX && isZ) {
            return Axis.Y;
        } else if(isY && isZ) {
            return Axis.X;
        }
        
        return null;
    }

    private boolean isDirectionOrIndex(int directionCoord, int indexCoord) {
        return Math.abs(directionCoord) > 0 || Math.abs(indexCoord) == 1;
    }
    
    private TileRotationType calcRotationType(TileIndex index) {
        TileRotationType rotType; 
        CubeSide side = index.getSide();
        
        if(direction.getX() > 0) {
            rotType = TileRotationType.CLOCKWISE;
        } else if(direction.getX() < 0) {
            rotType = TileRotationType.COUNTER_CLOCKWISE;
        } else     if(direction.getY() > 0) {
            rotType = TileRotationType.COUNTER_CLOCKWISE;
        } else if(direction.getY() < 0) {
            rotType = TileRotationType.CLOCKWISE;
        } else {
            if(direction.getZ() > 0) {
                rotType = TileRotationType.COUNTER_CLOCKWISE;
            } else {
                rotType = TileRotationType.CLOCKWISE;
            }
            if(side == CubeSide.RIGHT || side == CubeSide.LEFT) {
                if(rotType == TileRotationType.COUNTER_CLOCKWISE) {
                    rotType = TileRotationType.CLOCKWISE;
                } else {
                    rotType = TileRotationType.COUNTER_CLOCKWISE;
                }
            }
        }

        if(side == CubeSide.BACK || side == CubeSide.RIGHT || side == CubeSide.TOP) {
            if(rotType == TileRotationType.COUNTER_CLOCKWISE) {
                rotType = TileRotationType.CLOCKWISE;
            } else {
                rotType = TileRotationType.COUNTER_CLOCKWISE;
            }
        }
        
        return rotType;
    }

    public Vector3i asVector() {
        return direction;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((direction == null) ? 0 : direction.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IndexStep other = (IndexStep) obj;
        if (direction == null) {
            if (other.direction != null)
                return false;
        } else if (!direction.equals(other.direction))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TileIndexStep [direction=" + direction + "]";
    }

}
