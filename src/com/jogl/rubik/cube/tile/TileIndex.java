package com.jogl.rubik.cube.tile;

import static com.jogl.rubik.math.NumberUtils.isEven;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jogamp.opengl.math.Quaternion;
import com.jogl.rubik.cube.CubeSide;
import com.jogl.rubik.math.rotation.Rotation;
import com.jogl.rubik.math.vector.Vector3i;
import com.jogl.rubik.util.cache.impl.GeneralKey;

/**
 * The purpose of this class is to be able to index {@link Tile}s.
 * How this index works is not trivial so here is an explanation: <br/>
 * First solution was to store the tiles' index like this (3x3x3 cube):
 * <ul>
 *   <li>
 *      side=FRONT, row=0, column=0 (this index is for the tile at the FRONT, top-left corner)
 *   </li>
 *   <li>
 *      side=FRONT, row=0, column=1 (this is for the next tile in the row)
 *   </li>
 *   <li>
 *      side=FRONT, row=0, column=2 (last tile in the first row)
 *   </li>
 *   <li>
 *      side=FRONT, row=1, column=0
 *   </li>
 *   <li>
 *      side=FRONT, row=1, column=1
 *   </li>
 *   <li>
 *      side=FRONT, row=1, column=2
 *   </li>
 *   <li>
 *      etc...
 *   </li>
 * </ul>
 * The problem with this approach was that how are we going to rotate the tiles with
 * this representation? For instance if we want to rotate tile with index (side=FRONT, row=0, column=0)
 * to RIGHT side then it is very hard to determine the new row and column indices.
 * This applies to all sides. So the main problem was that it's very hard to rotate the tiles.
 * Not impossible but it would probably need a tremendous amount of 'if' statements.
 * Another problem was that it was a little bit complicated to calculate
 * the world position of a tile on the scene.  <br/>
 * <br/>
 * The second solution solved every problem listed above:<br/>
 * In this approach we store x,y,z coordinate-like values.
 * For instance this is how the FRONT side tiles' index (x,y,z accordingly)
 * look like for a 3x3x3 Rubik's Cube:
 * <pre>
 * {@code
 * |--------|--------|--------|
 * |-2, 2, 1| 0, 2, 1| 2, 2, 1|
 * |--------|--------|--------|
 * |-2, 0, 1| 0, 0, 1| 2, 0, 1|
 * |--------|--------|--------|
 * |-2,-2, 1| 0,-2, 1| 2,-2, 1|
 * |--------|--------|--------|
 * }
 * </pre>
 * Now we have a nice, symmetric structure. Note how the x,y,z coordinates
 * corresponds to their world position (and from these indices it's very easy to calculate the world coordinates).
 * If we want to write down the BACK side index coordinates then we
 * would get almost the same coordinates, but z would be -1.
 * For the RIGHT side the x coordinate would be 1 and for the LEFT side
 * x=-1. For the TOP side the y=1 and for the BOTTOM side y=-1.
 * So the coordinate that represents the tile's side is always
 * 1 or -1 and the other coordinates are always divisible by 2.
 * Due to the fact that these coordinates corresponds to the world coordinates
 * rotating tiles are very easy. Basically we can use the rotation functions
 * that we have in 3D, so all we need is an axis and a direction (+/-90 degree).
 * <br/>
 * For instance:
 * <ul>
 *   <li>
 *      Rotating tile (2,-2, 1) around the Y axis by +90 degree => (1,-2,-2)
 *      which means that the tile will be at the RIGHT side (x==1)
 *   </li>
 *   <li>
 *      Rotating tile (-2, 0, 1) around the X axis by -90 degree => (-2, 1, 0)
 *      which means that the tile will be at the TOP side (y==1)
 *   </li>
 * </ul>
 *
 * This solution is analogous to bigger Rubik's Cube sizes, if the size is odd.
 * In case the size is even then if we included 0 then the structure would not
 * be symmetric. We can overcome this problem very easily by omitting 0.
 * For instance this is how the FRONT side of a 4x4x4 Rubik's Cube looks like:
 * <pre>
 * {@code
 * |--------|--------|--------|--------|
 * |-4, 4, 1|-2, 4, 1| 2, 4, 1| 4, 4, 1|
 * |--------|--------|--------|--------|
 * |-4, 2, 1|-2, 2, 1| 2, 2, 1| 4, 2, 1|
 * |--------|--------|--------|--------|
 * |-4,-2, 1|-2,-2, 1| 2,-2, 1| 4,-2, 1|
 * |--------|--------|--------|--------|
 * |-4,-4, 1|-2,-4, 1| 2,-4, 1| 4,-4, 1|
 * |--------|--------|--------|--------|
 * }
 * </pre>
 * 
 * @see Tile
 * @see CubeSide
 */
public final class TileIndex extends GeneralKey {
    
    public static final int SIZE = 3;
    
    final Vector3i coords;

    private List<Float> cachedWorldCoords;
    private Quaternion cachedRotationQuaternion;

    /**
     * Creates a TileIndex from the given cubeSide's rotation and
     * the given x,y coordinate indices.
     * @param cubeSide
     * @param x
     * @param y
     */
    public TileIndex(CubeSide cubeSide, int x, int y) {
        // First we fill the index as if it was at the FRONT side... 
        Vector3i coords = new Vector3i(x, y, 1);
        // ... and then rotate this index to the given cubeSide. 
        Rotation sideRot = cubeSide.getSideRotation();
        coords = coords.rotate(sideRot);

        this.coords = coords;
        initCache();
    }
    
    public TileIndex(Vector3i coords) {
        this.coords = coords;
        initCache();
    }
    
    private void initCache() {
        cachedWorldCoords = new ArrayList<>(SIZE);
        Collections.addAll(cachedWorldCoords, null, null, null);
    }
    
    
    /**
     * Returns a copy of this TileIndex in which
     * it is rotated by the given rotation.
     * @param rotation
     * @return
     */
    public TileIndex rotate(Rotation rotation) {
        Vector3i rotatedIndex = coords.rotate(rotation);
        return new TileIndex(rotatedIndex);
    }

    public IndexStep calcIndexStep(TileIndex index2) {
        return new IndexStep(this, index2);
    }
    
    public Set<TileIndex> collectIndices(IndexStep indexStep, TileIndexRange indexRange) {
        Set<TileIndex> collectedIndices = new HashSet<>();

        if(indexStep.isValid()) {
            TileIndex nextTileIndex = applyStep(indexStep);
            while(nextTileIndex.isWithinRange(indexRange)) {
                if(nextTileIndex.isValidIndex(indexRange)) {
                    collectedIndices.add(nextTileIndex);
                }
                nextTileIndex = nextTileIndex.applyStep(indexStep);
            }
        }
        
        return collectedIndices;
    }
    
    public boolean isWithinRange(TileIndexRange indexRange) {
        for (int i = 0; i < size(); i++) {
            int coord = get(i);
            if(!indexRange.isCoordWithinRange(coord)) {
                return false;
            }
        }

        return true;
    }
    
    /**
     * Returns a copy of a new TileIndex in which the given
     * indexStep is applied to this TileIndex.
     * @param indexStep
     * @return
     */
    public TileIndex applyStep(IndexStep indexStep) {
        Vector3i newIndexVector = coords.add(indexStep.asVector());
        return new TileIndex(newIndexVector);
    }

    public Set<IndexStep> getPossibleSteps() {
        Set<IndexStep> possibleSteps = new HashSet<>();
        for (int i = 0; i < size(); i++) {
            int indexCoord = get(i);
            if(isEven(indexCoord)) {
                Vector3i possibleStepVect = new Vector3i();
                possibleStepVect = possibleStepVect.set(i, IndexStep.AMOUNT);
                possibleSteps.add(new IndexStep(possibleStepVect));
            }
        }
        
        return possibleSteps;
    }
    
    
    public boolean isValidIndex(TileIndexRange indexRange) {
        int oneCount = 0;
        
        for (int i = 0; i < size(); i++) {
            int indexCoord = get(i);
            if(!isValidCoord(indexCoord, indexRange)) {
                return false;
            }
            if(isOne(indexCoord)) {
                oneCount += 1;
            }
        }
        
        if(oneCount != 1) {
            return false;
        }
        
        return true;
    }

    private boolean isValidCoord(int indexCoord, TileIndexRange indexRange) {
        return indexRange.contains(indexCoord) || isOne(indexCoord);
    }
    
    public boolean isOne(int indexCoord) {
        return Math.abs(indexCoord) == 1;
    }

    public Rotation getSideRotation() {
        CubeSide cubeSide = getSide();
        return cubeSide.getSideRotation();
    }
    
    public CubeSide getSide() {
        return CubeSide.getSideFor(this);
    }
    
    public Quaternion calcRotationQuaternion() {
        if(cachedRotationQuaternion == null) {
            cachedRotationQuaternion = getSideRotation().asQuaternion();
        }
        return cachedRotationQuaternion;
    } 
   
    public float calcWorldX(int cubeSize) {
        return getOrCalcWorldCoord(0, cubeSize);
    }
    
    public float calcWorldY(int cubeSize) {
        return getOrCalcWorldCoord(1, cubeSize);
    }
    
    public float calcWorldZ(int cubeSize) {
        return getOrCalcWorldCoord(2, cubeSize);
    }
    
    private float getOrCalcWorldCoord(int coordIndex, float cubeSize) {
        Float worldCoord = cachedWorldCoords.get(coordIndex);
        
        if(worldCoord == null) {
            worldCoord = calcWorldCoord(coords.get(coordIndex), cubeSize);
            cachedWorldCoords.add(coordIndex, worldCoord);
        }
        
        return worldCoord;
    }
    
    private float calcWorldCoord(int coord, float cubeSize) {
        // If the given coordinate is 1 or -1 then
        // no further calculation is needed.
        if(isOne(coord)) {
            return coord;
        }
        
        float tileSize = 1.0f/cubeSize;
        float worldCoord = coord*tileSize;
        if(isEven(cubeSize)) {
            worldCoord += Math.signum(coord)*-tileSize;
        }
       
        return worldCoord;
    }
    
    public int getX() {
        return coords.getX();
    }
    
    public int getY() {
        return coords.getY();
    }
    
    public int getZ() {
        return coords.getZ();
    }
    
    public int get(int i) {
        return coords.get(i);
    }
    
    public Vector3i asVector() {
        return coords;
    }
    
    public int size() {
        return SIZE;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((coords == null) ? 0 : coords.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TileIndex other = (TileIndex) obj;
        if (coords == null) {
            if (other.coords != null)
                return false;
        } else if (!coords.equals(other.coords))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TileIndex " + coords;
    }
    
}
