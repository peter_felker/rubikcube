How to rotate tiles:
 - First you have to press the left mouse button on a tile (let's call it T1)
 - Then holding down the left mouse button you have to
   move the cursor to another tile (which must be on the SAME SIDE as T1!)
 - Finally release the left mouse button.

Game menu:
 - The Rubik's Cube can be shuffled by using the following menu: Game->Shuffle.
 - In the Game->Options menu you can change the game speed
   along with the mouse sensitivity.
   If you modify the Rubik's Cube size then it will only take effect when you start a New Game!

Controls:
 - Left mouse button: Rotating tiles.
 - Wheel: Zoom-in or out
 - Wheel pushed: rotate Rubik's Cube
 - Right mouse button: rotate Rubik's Cube by Z axis;
