### About

This is a basic Rubik's Cube game written in Java, using OpenGL for rendering ([JOGL](http://jogamp.org/jogl/www/)).

### Playing with the game

After downloading the game ("rubikcube v1.0.zip"), you can start it with "rubikcube v1.0/rubikcube.jar".

### Requirements

* Java 8 installed on your machine.
* Video card with at least OpenGL 3 support.